import json
from pathlib import Path

from pydantic import (
    BaseSettings,
    DirectoryPath,
    validator
)


class Settings(BaseSettings):
    DEBUG: bool = False
    ROOT_DIR: DirectoryPath = Path(__file__).absolute().parent
    RES_DIR: DirectoryPath = "res"

    @validator("RES_DIR", pre=True)
    def apply_root(cls, v, values):
        if root_dir := values.get("ROOT_DIR"):
            res_path = root_dir / v
            res_path.mkdir(parents=True, exist_ok=True)
            return res_path
        return v

    LOGGING_CONFIG: dict = {}

    @validator("LOGGING_CONFIG")
    def load_config(cls, v, values):
        res_dir = values.get("RES_DIR")
        logging_config_path = res_dir / "logging.json"
        if logging_config_path:
            with open(logging_config_path, "r") as f:
                v = json.load(f)
            assert v
        return v

    class Config:
        env_file = ".env"

    PULSE_MAX_ITEMS: int = 15
    PULSE_MAX_DELAY: int = 5
    PULSE_MAX_GAP_TIME: int = 10


settings = Settings()
