from shinka.utils.common import init_logger

from shinka.crawler.validator.dictionary import DictionaryValidator
from shinka.utils.validator import ValidationError, Severity


logger = init_logger(__name__)


def check_for_presence(cls, attrs, strict=False):
    """Checks for the existence of the attr set in the given class.

    Since ``None``, ``[]`` and ``{}`` are all falsy,
    performing a falsy check is sufficient.
    """
    all_present = list(
        map(lambda attr: bool(getattr(cls, attr)), attrs))
    return is_all_successful(all_present)


def stretch(base, new):
    return " > ".join((base, new))


def is_all_successful(bool_list):
    """Returns ``False`` if at least one item in the list is falsy.

    Checks for any falsy value in an array, ``any`` returns True
    so that we have to ``not`` it.
    """
    return not any([not i for i in bool_list])


def valmsg(path, obj):
    try:
        getattr(obj, "serialize")
        content = f"{type(obj)} {obj.serialize()}"
    except AttributeError:
        content = str(obj)
    return f"[{path}] {content}"


class CambridgeValidator(DictionaryValidator):
    def __init__(self, entity):
        super().__init__(entity)

    def _record(self, path, message, severity):
        if not self.errors.get(path):
            self.errors[path] = list()

        self.errors[path].append({
            'message': message,
            'severity': severity
        })

    def _review_error(self, error):
        severity = error.get('severity', Severity.ERROR)
        message = error.get('message', '')
        logger.log(severity, message)
        return not severity >= Severity.ERROR

    def _review(self, strict=True):
        if not self.entity.sources:
            msg = f"The word {self.entity.hint} doesn't exist in Cambridge."
            if strict:
                raise ValidationError(msg)
            else:
                logger.warning(msg)
            return False

        acceptable = True
        for path, errors in self.errors.items():
            results = [
                self._review_error(e)
                for e in errors
            ]
            acceptable = is_all_successful(results)

        if not acceptable:
            raise ValidationError("A broken word has been fetched.")
        logger.warning(f"Successfully reviewed {self.entity.word}")
        return True

    def _validate_description(self, desc, path) -> bool:
        logger.debug(valmsg(path, desc))
        if not desc.definition:
            self._record(
                path, valmsg(path, "Missing definition"), Severity.ERROR)
            return False
        if not desc.examples:
            self._record(
                path, valmsg(path, "No examples"), Severity.WARNING)
        if not desc.attrs:
            self._record(
                path, valmsg(path, "No attributes"), Severity.INFO)
        return True

    def _validate_descriptor(self, descriptor, path) -> bool:
        logger.debug(valmsg(path, descriptor))
        if not descriptor.descriptions:
            self._record(
                path, valmsg(path, "Missing description"), Severity.ERROR)
            return False
        if not descriptor.related_topics:
            self._record(
                path, valmsg(path, "No related topics"), Severity.INFO)

        return is_all_successful([
            self._validate_description(
                description, stretch(path, f"description#{index + 1}")
            ) for index, description in enumerate(descriptor.descriptions)
        ])

    def _validate_entry(self, entry, path) -> bool:
        logger.debug(valmsg(path, entry))
        # Checking if no pronunciation found is already included here
        if check_for_presence(
                # entry, ["type", "pronunciations", "descriptors"]
                entry, ["pronunciations", "descriptors"]
                ) is not True:
            self._record(
                path,
                valmsg(path, "Missing core fields: type, pron, or descriptos"),
                Severity.ERROR)
            return False
        # Validate all the descriptors in the source
        return is_all_successful([
            self._validate_descriptor(
                descriptor, stretch(path, f"descriptor#{index + 1}")
            ) for index, descriptor in enumerate(entry.descriptors)
        ])

    def _validate_src(self, src, path) -> bool:
        logger.debug(valmsg(path, src))
        if not src:
            self._record(
                path, valmsg(path, "No entries"), Severity.INFO)
        return is_all_successful([
            self._validate_entry(entry, stretch(path, f"entry#{index + 1}"))
            for index, entry in enumerate(src)
        ])

    def _validate_samples(self, samples, path) -> bool:
        pass

    def validate(self, strict=True) -> bool:
        _word = self.entity.word or f"{self.entity.word.hint} (unsure)"
        logger.info(f"Validating {_word}")
        cam_keys = {
            k: v
            for k, v in self.entity.sources.items() if "cambridge_" in k
        }
        for k, v in cam_keys.items():
            self._validate_src(
                v,
                stretch(self.entity.word, k)
            )
        # self._validate_samples(
        #     srckeys.get("cambridge_corpus", None),
        #     stretch(word.word, "cambridge_corpus")
        # )
        return self._review(strict)
