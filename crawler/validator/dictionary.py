class DictionaryValidator:
    def __init__(self, entity, errors=None):
        self._entity = entity
        self._errors = errors or dict()

    @property
    def entity(self):
        return self._entity

    @entity.setter
    def entity(self, value):
        self._entity = value

    @property
    def errors(self):
        return self._errors

    @errors.setter
    def errors(self, value):
        self._errors = value

    def validate(self):
        pass
