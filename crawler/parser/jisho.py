import bs4
import json
import os

from shinka.shared.fetcher import SimpleHTTPFetcher
from shinka.crawler.entity.japanese import Entity, Attributes, \
    Relations, Sense, Descriptor
from shinka.crawler.parser.dictionary import DictionaryParser
from shinka.utils.common import init_logger, njoin, truthy_dict


logger = init_logger(__name__)
RESDIR = njoin(os.path.dirname(__file__), '../res')


RES = dict()
with open(njoin(RESDIR, 'jisho.json'), 'r', encoding='utf8') as f:
    RES = json.load(f)


API = RES['api-pattern']
HTML = RES['html-pattern']


class JishoParser(DictionaryParser):
    def __init__(self, word):
        super().__init__(word)
        self.rawdict = None
        self.soup = None
        self.output = None

    def parse(self):
        resp = SimpleHTTPFetcher.fetch(API.format(word=self.word))
        dict_ = json.loads(resp)
        dict_ = list(filter(lambda i: i.get('slug') == self.word, dict_))
        self.rawdict = dict_.get('data')
        html = SimpleHTTPFetcher.fetch(HTML.format(word=self.word))
        self.soup = bs4.BeautifulSoup(html, 'html.parser')

    @staticmethod
    def _parse_examples(node):
        """Parses additional examples from the htmtl Jisho."""
        pass

    @staticmethod
    def _parse_sense(dict_):
        defns = dict_.get('english_definitions')
        types = dict_.get('parts_of_speech')
        examples = list()
        attrs = Attributes(
            tags=dict_.get('tags', list()), info=dict_.get('info', list()))
        rels = Relations(
            dict_.get('see_also', list()), dict_.get('antonyms', list()))
        others = truthy_dict({
            'links': dict_.get('links', list()),
            'restrictions': dict_.get('restrictions', list()),
            'source': dict_.get('source', list())
        })
        return Sense(defns, types, examples, attrs, rels, others)

    @staticmethod
    def _parse_descriptor(dict_):
        slug = dict_.get('slug')
        tags = dict_.get('tags', list())
        jlpt = dict_.get('jlpt', list())
        aliases = dict_.get('japanese')
        senses = [
            JishoParser._parse_sense(s)
            for s in dict_.get('senses', list())
        ]
        others = dict_.get('attributes')
        attrs = Attributes(tags=tags, jlpt=jlpt, others=others)
        return Descriptor(slug, aliases, senses, attrs)

    def _parse_jsonapi(self):
        if not self.rawdict:
            return Entity(self.word)
        descriptors = [
            JishoParser._parse_descriptor(d)
            for d in self.rawdict
        ]
        return Entity(self.word, descriptors)

    def _parse_html(self):
        pass
