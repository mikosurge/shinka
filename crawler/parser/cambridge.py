import json
import os
from bs4 import BeautifulSoup

from shinka.shared.fetcher import SimpleHTTPFetcher
from shinka.crawler.entity.english import Descriptor, Description,\
    Entry, Entity, Attributes, Relations
from shinka.crawler.parser.dictionary import DictionaryParser
from shinka.utils.common import init_logger, njoin, truthy_list
from shinka.utils.soup import first, ftext, textlist
from shinka.shared.exporter import PlainExporter


logger = init_logger(__name__)
RESDIR = njoin(os.path.dirname(__file__), '../res')
TMPDIR = njoin(os.path.dirname(__file__), '../temp')


RES = dict()
with open(njoin(RESDIR, 'cambridge.json'), 'r', encoding='utf8') as f:
    RES = json.load(f)

SOURCES = RES['sources']
SELS = RES['selectors']


class CambridgeParser(DictionaryParser):
    def __init__(self, word):
        super().__init__(word)
        self.soup = None

    @staticmethod
    def _parse_rel_topic(node) -> dict:
        return {
            'name': node.get_text().strip(),
            'url': node.get('href')
        }

    @staticmethod
    def _parse_pron(node) -> dict:
        ENTRY = SELS['entry']
        uk = ', '.join(textlist(node, ENTRY['pron-uk']))
        us = ', '.join(textlist(node, ENTRY['pron-us']))
        return {'uk': uk, 'us': us}

    @staticmethod
    def _parse_attrs(node) -> Attributes:
        ATTR = SELS['attr']
        cefr = ftext(node, ATTR['cefr'])
        gram = truthy_list(
            [ftext(node, ATTR['gram'])]
        )  # Default to list
        region = ftext(node, ATTR['region'])
        usages = textlist(node, ATTR['usages'])
        var = ftext(node, ATTR['var'])
        return Attributes(cefr, gram, region, usages, var)

    @staticmethod
    def _parse_rels(node) -> Relations:
        RELS = SELS['rels']['template']
        synonyms = textlist(node, RELS.format(rel='synonyms')) + \
            textlist(node, RELS.format(rel='synonym'))
        antonyms = textlist(node, RELS.format(rel='opposites')) + \
            textlist(node, RELS.format(rel='opposite'))
        compare = textlist(node, RELS.format(rel='compare'))
        see = textlist(node, RELS.format(rel='see')) + \
            textlist(node, RELS.format(rel='see_also')) + \
            textlist(node, RELS.format(rel='see_at'))
        return Relations(synonyms, antonyms, compare, see)

    @staticmethod
    def _parse_description(node, phrase=None) -> Description:
        DESC = SELS['description']
        if not (deftn := ftext(node, DESC['definition'])):
            return None
        exmps = textlist(node, DESC['examples'])
        attrs = CambridgeParser._parse_attrs(node)
        rels = CambridgeParser._parse_rels(node)
        return Description(deftn, exmps, attrs, rels, phrase)

    @staticmethod
    def _parse_phraseblock(node) -> Descriptor:
        PHBL = SELS['phraseblock']
        phrase = textlist(node, PHBL['header'])
        return CambridgeParser._parse_description(node, phrase)

    @staticmethod
    def _parse_descriptor(node) -> Descriptor:
        DSRT = SELS['descriptor']
        descs = truthy_list([
            CambridgeParser._parse_description(desc)
            for desc in node.select(DSRT['description'])
        ]) or list()

        phrases = [
            CambridgeParser._parse_phraseblock(ph)
            for ph in node.select(DSRT['phraseblock'])
        ] or list()
        descs += phrases

        if not descs:
            logger.error("Found empty description")
            return None

        gram = ftext(node, DSRT['gram'])
        guideword = ftext(node, DSRT['guideword'])
        rltps = list()
        maintopic = first(node, DSRT['rel-main'])
        if maintopic:
            rltps.append(
                CambridgeParser._parse_rel_topic(maintopic)
            )
            rltps += [
                CambridgeParser._parse_rel_topic(sub)
                for sub in node.select(DSRT['rel-sub'])
            ]
        if not first(node, DSRT['inner_rels']):
            rels = CambridgeParser._parse_rels(node)
        more_examples = textlist(node, DSRT['more_examples'])
        return Descriptor(descs, rltps, gram, guideword, rels, more_examples)

    @staticmethod
    def _parse_entry(node) -> Entry:
        ENTRY = SELS['entry']
        pron = CambridgeParser._parse_pron(node)
        type_ = ftext(node, ENTRY['type'])
        domain = ftext(node, ENTRY['domain'])
        descriptors = truthy_list([
            CambridgeParser._parse_descriptor(descriptor)
            for descriptor in node.select(ENTRY['descriptor'])
        ])
        usages = textlist(node, ENTRY['usages'])
        idioms = textlist(node, ENTRY['idioms'])
        phverbs = textlist(node, ENTRY['phrasalverbs'])
        return Entry(type_, pron, descriptors, usages, domain, idioms, phverbs)

    @staticmethod
    def _parse_corpus(node) -> dict:
        pass

    @staticmethod
    def _parse_cambridge_sources(sources: dict, node) -> dict:

        parsed = dict()
        for name, sel in sources.items():
            if src := first(node, sel):
                parsed[name] = truthy_list([
                    CambridgeParser._parse_entry(e)
                    for e in src.select(SELS['sources']['entry'])
                ])
        return parsed

    def parse(self, save_path=None) -> Entity:
        PATTERN = RES['pattern']
        html = SimpleHTTPFetcher.fetch(
            PATTERN.format(word=self.word),
            headers=RES["request"].get("headers", {})
        )
        if isinstance(save_path, str):
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            PlainExporter.export(
                njoin(save_path, f"{self.word}.html"), html
            )
        self.soup = BeautifulSoup(html, 'html.parser')
        if word := ftext(self.soup, SELS['word-content']):
            parsed_sources = CambridgeParser._parse_cambridge_sources(
                SOURCES, self.soup)
            if self.word != word:
                logger.warning(f"Hinted word is different from the \
search result: {self.word} => {word}")
            entity = Entity(word, ensured=True)
            for k, v in parsed_sources.items():
                entity.set_source(k, v)
            return entity
        else:
            return Entity(self.word)
