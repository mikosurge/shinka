class DictionaryParser:
    def __init__(self, word):
        self._word = word

    @property
    def word(self):
        return self._word

    @word.setter
    def word(self, value):
        self._word = value

    def parse(self):
        pass
