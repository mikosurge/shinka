from shinka.utils.common import truthy_dict, stringify


class Word:
    """
    The generalized word unit that supports core aspects.

    Attributes:
        word (str): The word itself.
        type_ (str): The word type (e.g., noun, verb).
        attrs (list<str>): Any other attributes a word might have
            (e.g., C2, singular).
        pronunciations (dict): All forms of pronunciations (us, uk, etc.)
        source (str): Where the word is refered (e.g., cambridge, oxford).
        definition (str): The meaning of the word.
        examples (list<str>): All given examples of the word from the source.
        relations (dict): All synonyms, antonyms and words that are related.
        related_topics (list<str>): Topics that the word are in.
        counterparts (dict): The word in other languages.
        contributor (str): Which person or series let you know the word.
        authentic (bool): Indicates if the word is from an authentic source.
            Could also means the word might be incomplete.
    """
    def __init__(
        self,
        word: str,
        type_: str,
        attrs: list,
        pronunciations: dict,
        source: str,
        definition: str,
        examples: list,
        relations: dict,
        related_topics: list,
        counterparts: dict,
        contributor: str,
        authentic: bool = True,
        incomplete: bool = False,
    ):
        self.word = word
        self.type = type_
        self.attrs = attrs
        self.pronunciations = pronunciations
        self.source = source
        self.definition = definition
        self.examples = examples
        self.relations = relations
        self.related_topics = related_topics
        self.counterparts = counterparts
        self.contributor = contributor
        self.authentic = authentic
        self.incomplete = incomplete

    @classmethod
    def from_dict(cls, dict_):
        word = dict_.get('word')
        type_ = dict_.get('type')
        attrs = dict_.get('attrs')
        pronunciations = dict_.get('pronunciations')
        source = dict_.get('source')
        definition = dict_.get('definition')
        examples = dict_.get('examples')
        relations = dict_.get('relations')
        related_topics = dict_.get('related_topics')
        counterparts = dict_.get('counterparts')
        contributor = dict_.get('contributor')
        authentic = dict_.get('authentic')
        incomplete = dict_.get('incomplete')
        return cls(word, type_, attrs, pronunciations,
                   source, definition, examples,
                   relations, related_topics, counterparts,
                   contributor, authentic, incomplete)

    def serialize(self):
        return truthy_dict(self.__dict__)

    def stringify(self):
        return stringify(self.serialize())
