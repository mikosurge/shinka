import asyncio
import json
import os
import re

from shinka.crawler.parser.cambridge import CambridgeParser as CP
from shinka.crawler.validator.cambridge \
    import CambridgeValidator, ValidationError
from shinka.shared.exporter import PlainExporter as PE
from shinka.utils.common import init_logger, njoin, truthy_list
from shinka.utils.pulse import random_pulse


logger = init_logger('crawler.bulk')
TMPDIR = njoin(os.path.dirname(__file__), 'temp')
RESDIR = njoin(os.path.dirname(__file__), 'res')
SRCDIR = njoin(os.path.dirname(__file__), 'src')
KEEPCHARS = [' ', '-', '\'', '_']


def omit_special_chars(str_):
    return ''.join(
        c for c in str_
        if (c.isalnum() or c in KEEPCHARS)
    )


def fetch_word(word):
    entity = CP(word).parse(save_path=SRCDIR)
    if CambridgeValidator(entity).validate():
        PE.export(njoin(TMPDIR, f"{word}.json"), entity.stringify())
    return entity


async def main():
    words = list()
    with open(njoin(RESDIR, '__items.json'), 'r', encoding='utf8') as f:
        words = json.load(f)

    words = truthy_list(words)
    # Remove slashes and brackets
    words = list(map(lambda i: re.sub(r'[\/\\\(\)]', '', i), words))
    # Keep only friendly characters
    words = list(map(omit_special_chars, words))
    # Substitute spaces and underlines with dashes
    words = list(map(lambda i: re.sub(r'[\s\'_]', '-', i), words))

    try:
        await random_pulse(fetch_word, words, TMPDIR)
    except ValidationError:
        logger.error("Stopped due to a critical validation error.")


asyncio.run(main())
