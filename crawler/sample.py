import json
import os

from shinka.crawler.entity.english import Entity
from shinka.crawler.parser.cambridge import CambridgeParser  # noqa
from shinka.crawler.validator.cambridge import CambridgeValidator
from shinka.shared.exporter import PlainExporter as PE  # noqa
from shinka.utils.common import init_logger, njoin


TMPDIR = njoin(os.path.dirname(__file__), 'temp')
SRCDIR = njoin(os.path.dirname(__file__), 'src')
logger = init_logger('crawler.sample')

word = 'check'


if not os.path.exists(TMPDIR):
    os.makedirs(TMPDIR)

cambridge_parser = CambridgeParser(word)
entity = cambridge_parser.parse(save_path=SRCDIR)
PE.export(njoin(TMPDIR, f'{word}.json'), entity.stringify())

with open(njoin(TMPDIR, f'{word}.json'), 'r', encoding='utf8') as f:
    entity_dict = json.load(f)

entity = Entity.from_dict(entity_dict)
cambridge_validator = CambridgeValidator(entity)
cambridge_validator.validate(strict=False)

generated_words = list(entity.generate_words())
generated_dict = [
    word.serialize()
    for word in generated_words
]

with open(njoin(os.path.dirname(__file__), f'{word}-generated.json'),
          'w+', encoding='utf8') as f:
    json.dump(generated_dict, f, indent=2, ensure_ascii=False)
