from typing import Generator

from shinka.crawler.unit.word import Word
from shinka.utils.common import truthy_dict, stringify


class Attributes:
    def __init__(
            self,
            cefr: str = None,
            gram: list = None,
            region: str = None,
            usages: list = None,
            var: str = None,
            domain: str = None,
            ):
        self.cefr = cefr
        self.gram = gram or list()
        self.domain = domain
        self.region = region
        self.usages = usages or list()
        self.var = var

    @classmethod
    def from_dict(cls, dict_=dict()):
        cefr = dict_.get('cefr')
        gram = dict_.get('gram')
        domain = dict_.get('domain')
        region = dict_.get('region')
        usages = dict_.get('usages', list())
        var = dict_.get('var')
        return cls(cefr, gram, region, usages, var, domain)

    def merge(self, _attrs):
        for key, value in _attrs.items():
            if not value:
                continue
            if (a := getattr(self, key, None)):
                if isinstance(a, list):
                    a += [
                        i for i in value
                        if i not in a
                    ]
        return self

    def serialize(self):
        return truthy_dict(self.__dict__)

    def stringify(self):
        return stringify(self.serialize())


class Relations:
    def __init__(self,
                 synonyms: list = None,
                 antonyms: list = None,
                 compare: list = None,
                 see: list = None):
        self.synonyms = synonyms or list()
        self.antonyms = antonyms or list()
        self.compare = compare or list()
        self.see = see or list()

    @classmethod
    def from_dict(cls, dict_=dict()):
        synonyms = dict_.get('synonyms', list())
        antonyms = dict_.get('antonyms', list())
        compare = dict_.get('compare', list())
        see = dict_.get('see', list())
        return cls(synonyms, antonyms, compare, see)

    # This function has problems
    def merge(self, _attrs):
        for key, value in _attrs.items():
            if not value:
                continue
            if (a := getattr(self, key, None)):
                if isinstance(a, list):
                    a += [
                        i for i in value
                        if i not in a
                    ]
        return self

    def serialize(self):
        return truthy_dict(self.__dict__)

    def stringify(self):
        return stringify(self.serialize())


class Description:
    def __init__(
            self,
            definition: str = None,
            examples: list = None,
            attrs=None,
            rels=None,
            phrase=None,
            ):
        self._definition = definition
        self._examples = examples or list()
        self._attrs = attrs
        self._rels = rels
        # Phrase only exists if the description is in a phrase block
        self._phrase = phrase

    @property
    def definition(self):
        return self._definition

    @definition.setter
    def definition(self, value):
        self._definition = value

    @property
    def examples(self):
        return self._examples

    @examples.setter
    def examples(self, value):
        self._examples = value

    @property
    def attrs(self):
        return self._attrs

    @attrs.setter
    def attrs(self, value):
        self._attrs = value

    @property
    def rels(self):
        return self._rels

    @rels.setter
    def rels(self, value):
        self._rels = value

    @property
    def phrase(self):
        return self._phrase

    @phrase.setter
    def phrase(self, value):
        self._phrase = value

    @classmethod
    def from_dict(cls, dict_):
        definition = dict_.get('definition')
        examples = dict_.get('examples', list())
        attrs = Attributes.from_dict(dict_.get('attrs', dict()))
        rels = Relations.from_dict(dict_.get('rels', dict()))
        phrase = dict_.get('phrase')
        return cls(definition, examples, attrs, rels, phrase)

    def serialize(self) -> dict:
        return truthy_dict({
            'phrase': self.phrase,
            'definition': self.definition,
            'examples': self.examples,
            'attrs': self.attrs.serialize(),
            'rels': self.rels.serialize()
        })

    def stringify(self):
        return stringify(self.serialize())

    def _generate(self, origin):
        origin['definition'] = self.definition
        origin['examples'] = self.examples
        origin['phrase'] = self.phrase
        origin['attrs'] = self.attrs.merge({
            "gram": origin.get('__gram'),
            "domain": origin.get('__domain'),
            "usages": origin.get('__usages')
        }).serialize()
        origin['rels'] = self.rels.merge({
            "rels": origin.get('__rels')
        }).serialize()
        yield Word.from_dict(origin)


class Descriptor:
    def __init__(
            self,
            descriptions: list = None,
            related_topics: list = None,
            gram: str = None,
            guideword: str = None,
            rels: Relations = None,
            more_examples: list = None,
            ):
        self._descriptions = descriptions
        self._related_topics = related_topics or list()
        self._gram = gram
        self._guideword = guideword
        self._rels = rels
        self._more_examples = more_examples

    @property
    def descriptions(self):
        return self._descriptions

    @descriptions.setter
    def descriptions(self, value):
        self._descriptions = value

    @property
    def related_topics(self):
        return self._related_topics

    @related_topics.setter
    def related_topics(self, value):
        self._related_topics = value

    @property
    def gram(self):
        return self._gram

    @gram.setter
    def gram(self, value):
        self._gram = value

    @property
    def guideword(self):
        return self._guideword

    @guideword.setter
    def guideword(self, value):
        self._guideword = value

    @property
    def rels(self):
        return self._rels

    @rels.setter
    def rels(self, value):
        self._rels = value

    @property
    def more_examples(self):
        return self._more_examples

    @more_examples.setter
    def more_examples(self, value):
        self._more_examples = value

    @classmethod
    def from_dict(cls, dict_):
        descriptions = [
            Description.from_dict(d)
            for d in dict_.get('descriptions', list())
        ]
        related_topics = dict_.get('related_topics', list())
        gram = dict_.get('gram')
        guideword = dict_.get('guideword')
        more_examples = dict_.get('more_examples')
        # rels will not be included here and move to attrs
        return cls(
            descriptions,
            related_topics,
            gram,
            guideword,
            None,
            more_examples
        )

    def serialize(self):
        return truthy_dict({
            'descriptions': [
                d.serialize()
                for d in self.descriptions
            ],
            'related_topics': self.related_topics,
            'gram': self.gram,
            'guideword': self.guideword,
            'rels': self.rels.serialize(),
            'more_examples': self.more_examples,
        })

    def stringify(self):
        return stringify(self.serialize())

    def _generate(self, origin):
        origin['related_topics'] = self.related_topics
        origin['guideword'] = self.guideword
        origin['more_examples'] = self.more_examples
        if self.gram:
            origin['__gram'] = self.gram
        if self.rels:
            origin['__rels'] = self.rels
        for desc in self.descriptions:
            yield from desc._generate(origin)


class Entry:
    def __init__(
            self,
            type_: str,
            pronunciations: dict,
            descriptors: list,
            usages: list = None,
            domain: str = None,
            idioms: list = None,
            phrasalverbs: list = None,
            ):
        self._type = type_
        self._pronunciations = pronunciations
        self._descriptors = descriptors
        self._usages = usages or list()
        self._domain = domain
        self._idioms = idioms or list()
        self._phrasalverbs = phrasalverbs or list()

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        self._type = value

    @property
    def pronunciations(self):
        return self._pronunciations

    @pronunciations.setter
    def pronunciations(self, value):
        self._pronunciations = value

    @property
    def descriptors(self):
        return self._descriptors

    @descriptors.setter
    def descriptors(self, value):
        self._descriptors = value

    @property
    def usages(self):
        return self._usages

    @usages.setter
    def usages(self, value):
        self._usages = value

    @property
    def domain(self):
        return self._domain

    @domain.setter
    def domain(self, value):
        self._domain = value

    @property
    def idioms(self):
        return self._idioms

    @idioms.setter
    def idioms(self, value):
        self._idioms = value

    @property
    def phrasalverbs(self):
        return self._phrasalverbs

    @phrasalverbs.setter
    def phrasalverbs(self, value):
        self._phrasalverbs = value

    @classmethod
    def from_dict(cls, dict_):
        type_ = dict_.get('type')
        pronunciations = dict_.get('pronunciations')
        domain = dict_.get('domain')
        idioms = dict_.get('idioms')
        phrasalverbs = dict_.get('phrasalverbs')
        descriptors = [
            Descriptor.from_dict(d)
            for d in dict_.get('descriptors', list())
        ]
        return cls(
            type_,
            pronunciations,
            descriptors,
            domain,
            idioms,
            phrasalverbs,
        )

    def serialize(self) -> dict:
        return truthy_dict({
            'type': self.type,
            'domain': self.domain,
            'idioms': self.idioms,
            'phrasalverbs': self.phrasalverbs,
            'pronunciations': self.pronunciations,
            'descriptors': [
                d.serialize() for d in self.descriptors
            ],
            'usages': self.usages
        })

    def stringify(self):
        return stringify(self.serialize())

    def _generate(self, origin):
        origin['type'] = self.type
        origin['idioms'] = self.idioms
        origin['phrasalverbs'] = self.phrasalverbs
        origin['pronunciations'] = self.pronunciations
        if self.domain:
            origin['__domain'] = self.domain
        if self.usages:
            origin['__usages'] = self.usages
        for descriptor in self.descriptors:
            yield from descriptor._generate(origin)


class Entity:
    """A Word dict corresponding to the Cambridge structure.
    Unlike the raw Word, which focuses on supporting Reminis,
    this class of Word does its best to reflect its origin.

    Attributes:
    ____word (str)
    |___sources (dict<list>)
        |___entries (list<obj>)
            |___type (str)
            |___domain (str) --> attrs
            |___idioms (list<str>)
            |___phrasalverbs (list<str>)
            |___pronunciations (dict<str>)
            |___usages (list<str>) --> attrs
            |___descriptors (list<obj>)
                |___descriptions (list<obj>)
                |   |___definition (str)
                |   |___examples (list<str>)
                |   |___attrs (dict<obj>)
                |   |   |___cefr (str)
                |   |   |___gram (list<str>)
                |   |   |___domain (str)
                |   |   |___region (str)
                |   |   |___usages (list<str>)
                |   |___rels (dict<obj>)
                |   |   |___synonyms (list<str>)
                |   |   |___antonyms (list<str>)
                |   |   |___compare (list<str>)
                |   |   |___see (list<str>)
                |___related_topics (list<str>)
                |___gram (str) --> attrs
                |___guideword (str)
            |_?_phraseblocks (dict)
                |___headers (list<str>)
                |___descriptions (list<obj>)

    """
    def __init__(self, hint_or_word, sources=None, ensured=False):
        self._hint = hint_or_word
        self._word = hint_or_word if ensured else None
        self._sources = sources or dict()

    @property
    def hint(self):
        return self._hint

    @hint.setter
    def hint(self, value):
        self._hint = value

    @property
    def word(self):
        return self._word

    @word.setter
    def word(self, value):
        self._word = value

    @property
    def sources(self):
        return self._sources

    def set_source(self, name, entries):
        self._sources[name] = entries

    def get_source(self, name) -> dict:
        return self._sources.get(name, None)

    @classmethod
    def from_dict(cls, dict_):
        sourcedict = dict_.get('sources', dict())
        word = dict_.get('word')
        sources = {
            key: [
                Entry.from_dict(e)
                for e in value
            ]
            for key, value in sourcedict.items()
        }
        return cls(word, sources, ensured=bool(word))

    def serialize(self) -> dict:
        return({
            'word': self.word,
            'sources': {
                key: [
                    e.serialize()
                    for e in value
                ]
                for key, value in self._sources.items()
            }
        })

    def stringify(self):
        return stringify(self.serialize())

    def generate_words(self) -> Generator:
        """Returns a generator of definitions
            of the same word with different contents.
        """
        origin = {
            'word': self.word
        }
        for key in self.sources.keys():
            origin['source'] = key
            for entry in self.sources[key]:
                yield from entry._generate(origin)
