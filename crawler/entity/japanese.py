from typing import Generator
from shinka.crawler.unit.word import Word
from shinka.utils.common import truthy_dict, serialize


class Attributes:
    def __init__(self,
                 tags: list = None,
                 info: list = None,
                 jlpt: list = None,
                 others: dict = None):
        self.tags = tags or list()
        self.info = info or list()
        self.jlpt = jlpt or list()
        self.others = others or dict()

    @classmethod
    def from_dict(cls, dict_=dict()):
        """tags, jlpt, and info can be converted into attrs.
        """
        tags = dict_.get('tags', list())
        info = dict_.get('info', list())
        jlpt = dict_.get('jlpt', list())
        others = dict_.get('others', dict())
        return cls(tags, info, jlpt, others)

    def serialize(self):
        return truthy_dict(self.__dict__)

    def stringify(self):
        return stringify(self.serialize())


class Relations:
    def __init__(self,
                 antonyms: list = None,
                 see: list = None):
        self.antonyms = antonyms or list()
        self.see = see or list()

    @classmethod
    def from_dict(cls, dict_=dict()):
        antonyms = dict_.get('antonyms', list())
        see = dict_.get('see', list())
        return cls(antonyms, see)

    def serialize(self):
        return truthy_dict(self.__dict__)

    def stringify(self):
        return stringify(self.serialize())


class Sense:
    """A Sense in Japanese may contain more than one definition,
    as they look similar. The definition list itself is guaranteed
    to have at least one member.
    """
    def __init__(self,
                 definitions: list,
                 types: list,  # parts_of_speech
                 examples: list,
                 attrs: dict,  # tags, info
                 rels: dict,  # see_also, antonyms
                 others: dict):  # links, restrictions, source
        self._definitions = definitions
        self._types = types or list()
        self._examples = examples or list()
        self._attrs = Attributes.from_dict(attrs or dict())
        self._rels = Relations.from_dict(rels or dict())
        self._others = others or dict()

    @classmethod
    def from_dict(cls, dict_):
        definitions = dict_.get('definitions')
        if not definitions:
            raise ValueError("A Sense should have at least one definition")
        types = dict_.get('types')
        if not types:
            raise ValueError("A Sense should have at least one type")
        examples = dict_.get('examples', list())
        attrs = Attributes.from_dict(dict_.get('attrs'), dict())
        rels = Relations.from_dict(dict_.get('rels'), dict())
        others = dict_.get('others', dict())
        return cls(definitions, types, examples, attrs, rels, others)

    @property
    def definitions(self):
        return self._definitions

    @definitions.setter
    def definitions(self, val):
        self._definitions = val

    @property
    def types(self):
        return self._types

    @types.setter
    def types(self, val):
        self.types = val

    @property
    def examples(self):
        return self._examples

    @examples.setter
    def examples(self, val):
        self._examples = val

    @property
    def attrs(self):
        return self._attrs

    @attrs.setter
    def attrs(self, val):
        self._attrs = val

    @property
    def rels(self):
        return self._rels

    @rels.setter
    def rels(self, val):
        self._rels = val

    @property
    def others(self):
        return self._others

    @others.setter
    def others(self, val):
        self._others = val

    @classmethod
    def from_json(cls, json_):
        pass


    def serialize(self) -> dict:
        return truthy_dict({
            'definitions': self.definitions,
            'type': self.type,
            'examples': self.examples,
            'attrs': self.attrs.serialize(),
            'rels': self.rels.serialize(),
            'others': self.others,
        })

    def stringify(self):
        return stringify(self.serialize())


class Descriptor:
    def __init__(self, slug, aliases, senses, attrs):
        self._slug = slug
        self._aliases = aliases
        self._senses = senses
        self._attrs = attrs

    @classmethod
    def from_dict(cls, dict_):
        slug = dict_.get('slug')
        aliases = dict_.get('aliases')
        senses = [
            Sense.from_dict(s)
            for s in dict_.get('senses', list())
        ]
        attrs = [
            Attributes.from_dict(a)
            for a in dict_.get('attributes', list())
        ]
        return cls(slug, aliases, senses, attrs)

    @property
    def slug(self):
        return self._slug

    @slug.setter
    def slug(self, val):
        self._slug = val

    @property
    def aliases(self):
        return self._aliases

    @aliases.setter
    def aliases(self, val):
        self._aliases = val

    @property
    def senses(self):
        return self._senses

    @senses.setter
    def senses(self, val):
        self._senses = val

    @property
    def attrs(self):
        return self._attrs

    @attrs.setter
    def attrs(self, val):
        self._attrs = val


class Entity:
    """A JWord dict corresponding to the Jisho structure.
    Unlike the raw Word, which focuses on supporting Reminis,
    this class of Word does its best to reflect its origin.

    Attributes:
    ____word (str) -> word
    |___descriptors (list<obj>)
        |___slug (str)
        |___tags (list<str>) -> attrs
        |___jlpt (list<str>) -> attrs
        |___japanese (list<dict>) -> variants => pronunciations
        |   |___word (str)
        |   |___reading (str)
        |___senses (list<obj>)
        |   |___english_definitions (list<str>) -> definitions
        |   |___parts_of_speech (list<str>) -> type_
        |   |___examples (list<str>) -> get through HTML
        |   |___links (list<dict>) -> not common
        |   |   |___text (str)
        |   |   |___url (str)
        |   |___tags (list<str>) -> attrs
        |   |___restrictions (list) -> not common
        |   |___see_also (list<str>) -> relations > see
        |   |___antonyms (list<str>) -> relations > antonyms
        |   |___source (list) -> not common => source
        |   |___info (list) -> attrs
        |___attributes (dict<str>)

    Missing: examples, examples will go with a sense
    """
    def __init__(self, word, descriptors: list = list()):
        self._word = word
        self._descriptors = descriptors

    @property
    def word(self):
        return self._word

    @word.setter
    def word(self, val):
        self._word = val

    @property
    def descriptors(self):
        return self._descriptors

    @descriptors.setter
    def descriptors(self, val):
        self._descriptors = val
