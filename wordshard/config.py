import json
from pathlib import Path
from typing import Any, Dict

from pydantic import (
    BaseSettings,
    DirectoryPath,
    FilePath,
    HttpUrl,
    validator
)


class Settings(BaseSettings):
    ROOT_DIR: DirectoryPath = Path(__file__).absolute().parent
    RES_DIR: DirectoryPath = "res"
    SRC_DIR: DirectoryPath = "src"
    TMP_DIR: DirectoryPath = "tmp"

    @validator("RES_DIR", "SRC_DIR", "TMP_DIR", pre=True)
    def apply_root(cls, v, values):
        if root_dir := values.get("ROOT_DIR"):
            res_path = root_dir / v
            res_path.mkdir(parents=True, exist_ok=True)
            return res_path
        return v

    CAMBRIDGE_RESOURCE: FilePath = "cambridge.json"

    @validator("CAMBRIDGE_RESOURCE", pre=True)
    def apply_res(cls, v, values):
        if res_dir := values.get("RES_DIR"):
            return res_dir / v
        return v

    CAMBRIDGE_CONFIG: Dict[str, Any] = {}

    @validator("CAMBRIDGE_CONFIG", pre=True)
    def load_config(cls, v, values):
        cam_res = values.get("CAMBRIDGE_RESOURCE")
        with open(cam_res, "r") as f:
            v = json.load(f)
        assert v
        return v

    CAMBRIDGE_PATTERN: HttpUrl = None

    @validator("CAMBRIDGE_PATTERN")
    def load_pattern(cls, v, values):
        return values.get("CAMBRIDGE_CONFIG").get("pattern")

    CAMBRIDGE_SELECTORS: dict = {"key": "selectors"}
    CAMBRIDGE_REQUEST: dict = {"key": "request"}
    CAMBRIDGE_SOURCES: dict = {"key": "sources"}

    @validator(
        "CAMBRIDGE_SELECTORS",
        "CAMBRIDGE_REQUEST",
        "CAMBRIDGE_SOURCES",
    )
    def load_essential_configs(cls, v, values):
        cam_cfg = values.get("CAMBRIDGE_CONFIG")
        return cam_cfg.get(v["key"])


settings = Settings()
