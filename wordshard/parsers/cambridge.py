from bs4 import BeautifulSoup
from pathlib import Path
from typing import Any, Dict, List, Optional

from shinka.wordshard.config import settings
from shinka.wordshard.models.cambridge import (
    Entity,
    Sources,
    Entry,
    Descriptor,
    Description,
    Attributes,
    Relations,
)
from shinka.wordshard.parsers.dictionary import IDictionaryParser
from shinka.shared.exporter import PlainExporter
from shinka.shared.fetcher import SimpleHTTPFetcher
from shinka.utils.soup import first, ftext, textlist
from shinka.utils.common import init_logger
from shinka.utils.shared import trim_falsy


logger = init_logger(__name__)
RES = settings.RES_DIR
SRC = settings.SRC_DIR
SOURCES = settings.CAMBRIDGE_SOURCES
SELS = settings.CAMBRIDGE_SELECTORS


class CambridgeParser(IDictionaryParser):
    def __init__(self, word):
        super().__init__(word)

    @staticmethod
    def _parse_rel_topics(node: BeautifulSoup) -> Dict[str, str]:
        return {
            'name': node.get_text().strip(),
            'url': node.get('href')
        }

    @staticmethod
    def _parse_pron(node: BeautifulSoup) -> Dict[str, str]:
        ENTRY = SELS['entry']
        uk = ', '.join(textlist(node, ENTRY['pron-uk']))
        us = ', '.join(textlist(node, ENTRY['pron-us']))
        return trim_falsy({'uk': uk, 'us': us})

    @staticmethod
    def _parse_attrs(node: BeautifulSoup) -> Attributes:
        ATTR = SELS["attr"]
        cefr = ftext(node, ATTR["cefr"])
        gram = ftext(node, ATTR["gram"])
        domain = ftext(node, ATTR["domain"])
        region = ftext(node, ATTR["region"])
        usages = textlist(node, ATTR["usages"])
        var = ftext(node, ATTR["var"])
        return Attributes(
            cefr=cefr,
            gram=gram,
            domain=domain,
            region=region,
            usages=usages,
            var=var,
        )

    @staticmethod
    def _parse_rels(node: BeautifulSoup) -> Relations:
        RELS = SELS["rels"]["template"]
        synonyms = textlist(node, RELS.format(rel="synonyms")) + \
            textlist(node, RELS.format(rel="synonym"))
        antonyms = textlist(node, RELS.format(rel="opposites")) + \
            textlist(node, RELS.format(rel="opposite"))
        compare = textlist(node, RELS.format(rel="compare"))
        see = textlist(node, RELS.format(rel="see")) + \
            textlist(node, RELS.format(rel="see_also")) + \
            textlist(node, RELS.format(rel="see_at"))
        return Relations(
            synonyms=synonyms,
            antonyms=antonyms,
            compare=compare,
            see=see
        )

    @staticmethod
    def _parse_description(
            node: BeautifulSoup,
            phrase: Optional[List[str]] = None,
            ) -> Description:
        DESC = SELS["description"]
        if not (definition := ftext(node, DESC["definition"])):
            return None
        examples = textlist(node, DESC["examples"])
        attrs = CambridgeParser._parse_attrs(node)
        rels = CambridgeParser._parse_rels(node)
        return Description(
            definition=definition,
            examples=examples,
            attrs=attrs,
            rels=rels,
            phrase=phrase,
        )

    @staticmethod
    def _parse_phraseblock(node: BeautifulSoup) -> Descriptor:
        PHBL = SELS["phraseblock"]
        phrase = ftext(node, PHBL["header"])
        return CambridgeParser._parse_description(node, phrase)

    @staticmethod
    def _parse_descriptor(node: BeautifulSoup) -> Descriptor:
        DSRT = SELS["descriptor"]
        descriptions = trim_falsy([
            CambridgeParser._parse_description(desc)
            for desc in node.select(DSRT["description"])
        ])
        phrases = trim_falsy([
            CambridgeParser._parse_phraseblock(ph)
            for ph in node.select(DSRT["phraseblock"])
        ])
        descriptions += phrases
        if not descriptions:
            logger.error("Found empty description")
            return None

        gram = ftext(node, DSRT["gram"])
        guideword = ftext(node, DSRT["guideword"])
        maintopic = first(node, DSRT["rel-main"])
        related_topics = list()
        if maintopic:
            related_topics.append(
                CambridgeParser._parse_rel_topics(maintopic)
            )
            related_topics += [
                CambridgeParser._parse_rel_topics(sub)
                for sub in node.select(DSRT["rel-sub"])
            ]
        if not first(node, DSRT["inner_rels"]):
            rels = CambridgeParser._parse_rels(node)
        more_examples = textlist(node, DSRT["more_examples"])
        return Descriptor(
            descriptions=descriptions,
            related_topics=related_topics,
            gram=gram,
            guideword=guideword,
            rels=rels,
            more_examples=more_examples
        )

    @staticmethod
    def _parse_entry(node: BeautifulSoup) -> Entry:
        ENTRY = SELS["entry"]
        pronunciations = CambridgeParser._parse_pron(node)
        type = ftext(node, ENTRY["type"])
        domain = ftext(node, ENTRY["domain"])
        descriptors = trim_falsy([
            CambridgeParser._parse_descriptor(descriptor)
            for descriptor in node.select(ENTRY["descriptor"])
        ])
        usages = textlist(node, ENTRY["usages"])
        idioms = textlist(node, ENTRY["idioms"])
        phrasalverbs = textlist(node, ENTRY["phrasalverbs"])
        return Entry(
            type=type,
            pronunciations=pronunciations,
            domain=domain,
            descriptors=descriptors,
            usages=usages,
            idioms=idioms,
            phrasalverbs=phrasalverbs,
        )

    @staticmethod
    def _parse_corpus(node: BeautifulSoup) -> Any:
        pass

    @staticmethod
    def _parse_sources(
            sources: Dict[str, str],
            node: BeautifulSoup,
            ) -> Sources:
        parsed = dict()
        for name, sel in sources.items():
            if src := first(node, sel):
                parsed[name] = trim_falsy([
                    CambridgeParser._parse_entry(entrynode)
                    for entrynode in src.select(SELS["sources"]["entry"])
                ])
        return Sources(**parsed)

    def parse(
            self,
            *,
            offline: bool = False,
            src_dir: str = SRC,
            save_dir: str = RES,
            ) -> Entity:
        PATTERN = settings.CAMBRIDGE_PATTERN
        save_dir = Path(save_dir)
        if offline:
            with open(src_dir / f"{self.word}.html", "r") as f:
                html = f.read()
        else:
            html = SimpleHTTPFetcher.fetch(
                PATTERN.format(word=self.word),
                headers=settings.CAMBRIDGE_REQUEST.get("headers", {}),
            )
            save_dir.mkdir(parents=True, exist_ok=True)
            PlainExporter.export(
                save_dir / f"{self.word}.html", html
            )
        self.soup = BeautifulSoup(html, "html.parser")
        hint = None
        if not (word := ftext(self.soup, SELS["word-content"])):
            return Entity(word=word, hint=hint, sources=Sources())
        sources = CambridgeParser._parse_sources(SOURCES, self.soup)
        if word != self.word:
            logger.warning(
                "Hinted word is different from the search result: "
                f"{self.word} => {word}"
            )
            hint = self.word
        return Entity(word=word, hint=hint, sources=sources)
