import json
import pickle
import pydantic

from shinka.wordshard.config import settings
from shinka.wordshard.models.cambridge import Entity


TMPDIR = settings.TMP_DIR


def get_file_list():
    for i in TMPDIR.glob("**/*"):
        yield i.name


def load_from_json(filename):
    with open(TMPDIR / filename, "r") as f:
        return json.load(f)


def generate_words(entity_dict):
    entity = Entity.parse_obj(entity_dict)
    return [
        word.dict()
        for word in entity.generate_words()
    ]


def accumulate_words():
    filenames = get_file_list()
    all_words = list()
    for filename in filenames:
        if filename.startswith("__"):
            continue
        data = load_from_json(filename)
        try:
            words = generate_words(data)
            all_words += words
        except pydantic.ValidationError:
            continue
    return all_words


all_shards = accumulate_words()

with open("shards.pickle", "wb") as f:
    pickle.dump(all_shards, f)
