import asyncio
import json
import pydantic
import re

from shinka.wordshard.parsers.cambridge import CambridgeParser
from shinka.shared.exporter import PlainExporter
from shinka.utils.shared import trim_falsy
from shinka.utils.common import init_logger
from shinka.utils.pulse import random_pulse
from shinka.wordshard.config import settings


logger = init_logger("wordshard.bulk")
RESDIR = settings.RES_DIR
SRCDIR = settings.SRC_DIR
TMPDIR = settings.TMP_DIR


def omit_special_chars(str_):
    return ''.join(
        c for c in str_
        if (c.isalnum() or c in settings.KEEPCHARS)
    )


def fetch_word(word):
    entity = CambridgeParser(word).parse(offline=True)
    PlainExporter.export(TMPDIR / f"{word}.json", entity.json())
    return entity


async def main():
    words = list()
    with open(RESDIR / '__items.json', 'r', encoding='utf8') as f:
        words = json.load(f)

    words = trim_falsy(words)
    # Remove slashes and brackets
    words = list(map(lambda i: re.sub(r'[\/\\\(\)]', '', i), words))
    # Keep only friendly characters
    words = list(map(omit_special_chars, words))
    # Substitute spaces and underlines with dashes
    words = list(map(lambda i: re.sub(r'[\s\'_]', '-', i), words))

    try:
        await random_pulse(fetch_word, words, TMPDIR)
    except pydantic.ValidationError:
        logger.error("Stopped due to a critical validation error.")


asyncio.run(main())
