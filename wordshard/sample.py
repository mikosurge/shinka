import json

from shinka.wordshard.models.cambridge import Entity
from shinka.wordshard.config import settings
from shinka.wordshard.parsers.cambridge import CambridgeParser
from shinka.shared.exporter import PlainExporter
from shinka.utils.common import init_logger


logger = init_logger("crawler.sample")
word = "check"


cambridge_parser = CambridgeParser(word)
entity = cambridge_parser.parse(offline=True)
PlainExporter.export(settings.TMP_DIR / f"{word}.json", entity.json())

with open(settings.TMP_DIR / f"{word}.json", "r", encoding="utf8") as f:
    entity_dict = json.load(f)


entity = Entity.parse_obj(entity_dict)

generated_words = list(entity.generate_words())
generated_dict = [
    word.dict()
    for word in generated_words
]

with open(
        settings.ROOT_DIR / f"{word}-generated.json",
        "w+",
        encoding="utf8"
        ) as f:
    json.dump(generated_dict, f, indent=2, ensure_ascii=False)
