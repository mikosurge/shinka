import pydantic
from typing import (
    Any,
    AbstractSet,
    Generator,
    Mapping,
    Optional,
    Tuple,
    Union,
)
from pydantic.utils import ValueItems


TupleGenerator = Generator[Tuple[str, Any], None, None]
IntStr = Union[int, str]
AbstractSetIntStr = AbstractSet[IntStr]
MappingIntStrAny = Mapping[IntStr, Any]
_missing = object()


def is_falsy(value):
    if not getattr(value, "__dict__", None):
        return not value
    for k, v in value.__dict__.items():
        if isinstance(v, type):
            if not is_falsy(v):
                return False
        else:
            if v:
                return False
    return True


class BaseModel(pydantic.BaseModel):
    @classmethod
    def _get_value(
        cls,
        v: Any,
        to_dict: bool,
        by_alias: bool,
        include: Optional[Union['AbstractSetIntStr', 'MappingIntStrAny']],
        exclude: Optional[Union['AbstractSetIntStr', 'MappingIntStrAny']],
        exclude_unset: bool,
        exclude_defaults: bool,
        exclude_none: bool,
    ) -> Any:
        v = super()._get_value(
            v,
            to_dict=to_dict,
            by_alias=by_alias,
            exclude_unset=exclude_unset,
            exclude_defaults=exclude_defaults,
            include=include,
            exclude=exclude,
            exclude_none=exclude_none,
        )
        return v if v else None

    def dict(
        self,
        exclude_none: bool = True,
        **kwargs,
    ) -> Any:
        dict_ = super().dict(
            exclude_none=exclude_none,
            **kwargs,
        )
        return dict_

    def json(
        self,
        exclude_none: bool = True,
        **kwargs,
    ) -> str:
        return super().json(
            exclude_none=exclude_none,
            indent=2,
            ensure_ascii=False,
            **kwargs,
        )

    def _iter(
        self,
        to_dict: bool = False,
        by_alias: bool = False,
        include: Union['AbstractSetIntStr', 'MappingIntStrAny'] = None,
        exclude: Union['AbstractSetIntStr', 'MappingIntStrAny'] = None,
        exclude_unset: bool = False,
        exclude_defaults: bool = False,
        exclude_none: bool = False,
    ) -> 'TupleGenerator':
        """
        Since adding another exclusion option could be cumbersome.
        The exclude_none is modified for trimming all falsy values.
        So that we don't need to add exclude_falsy in all other functions.
        """
        allowed_keys = self._calculate_keys(
            include=include,
            exclude=exclude,
            exclude_unset=exclude_unset
        )
        if allowed_keys is None and not (
                to_dict or by_alias or exclude_unset
                or exclude_defaults or exclude_none):
            # huge boost for plain _iter()
            yield from self.__dict__.items()
            return

        value_exclude = ValueItems(self, exclude) if exclude else None
        value_include = ValueItems(self, include) if include else None

        for field_key, v in self.__dict__.items():
            if (
                (allowed_keys is not None and field_key not in allowed_keys)
                or (exclude_none and is_falsy(v))
                or (
                    exclude_defaults and getattr(
                        self.__fields__.get(field_key), 'default', _missing
                    ) == v
                )
            ):
                continue
            if by_alias and field_key in self.__fields__:
                dict_key = self.__fields__[field_key].alias
            else:
                dict_key = field_key
            if to_dict or value_include or value_exclude:
                v = self._get_value(
                    v,
                    to_dict=to_dict,
                    by_alias=by_alias,
                    include=(
                        value_include and value_include.for_element(field_key)
                    ),
                    exclude=(
                        value_exclude and value_exclude.for_element(field_key)
                    ),
                    exclude_unset=exclude_unset,
                    exclude_defaults=exclude_defaults,
                    exclude_none=exclude_none,
                )
            yield dict_key, v

    class Config:
        validate_assignment = True
