from pydantic import Field
from typing import Any, Dict, Generator, List, Optional

from shinka.wordshard.models.base import BaseModel
from shinka.wordshard.models.shard import WordShard
from shinka.utils.shared import merge


class Attributes(BaseModel):
    cefr: Optional[str] = Field(
        None, description="CEFR Level")
    gram: Optional[Any] = Field(
        None, description="Grammar annotation")
    domain: Optional[str] = Field(
        None, description="The domain which the word is commonly used")
    region: Optional[str] = Field(
        None, description="The geographic region the word is commonly used")
    var: Optional[str] = Field(
        None, description="The word's variants")
    # Usages is confirmed to be a list, i.e. [formal, approving]
    usages: Optional[List[str]] = Field(
        None, description="Common usages")


class Relations(BaseModel):
    synonyms: Optional[List[str]] = Field(
        None)
    antonyms: Optional[List[str]] = Field(
        None)
    compare: Optional[List[str]] = Field(
        None)
    see: Optional[List[str]] = Field(
        None)


class Description(BaseModel):
    definition: Optional[str] = Field(
        None)
    examples: Optional[List[str]] = Field(
        None)
    attrs: Optional[Attributes] = Field(
        None)
    rels: Optional[Relations] = Field(
        None)
    phrase: Optional[str] = Field(
        None)

    def _generate(self, origin):
        origin["definition"] = self.definition
        origin["examples"] = self.examples
        origin["phrase"] = self.phrase
        origin["attrs"] = merge(self.attrs, Attributes.parse_obj({
            "gram": origin.get("__gram"),
            "domain": origin.get("__domain"),
            "usages": origin.get("__usages"),
        })).dict()
        origin["rels"] = merge(
            self.rels, Relations.parse_obj(origin.get("__rels"))
        ).dict()
        yield WordShard.parse_obj(origin)


class Descriptor(BaseModel):
    descriptions: List[Description] = Field(
        ...)
    related_topics: Optional[List[Dict[str, str]]] = Field(
        None)
    gram: Optional[str] = Field(
        None)
    guideword: Optional[str] = Field(
        None)
    rels: Optional[Relations] = Field(
        None)
    more_examples: Optional[List[str]] = Field(
        None)

    def _generate(self, origin):
        origin["related_topics"] = self.related_topics
        origin["guideword"] = self.guideword
        origin["more_examples"] = self.more_examples
        origin["__gram"] = self.gram if self.gram else None
        origin["__rels"] = self.rels if self.rels else {}
        for desc in self.descriptions:
            yield from desc._generate(origin)


class Entry(BaseModel):
    type: Optional[str] = Field(
        None)
    pronunciations: Dict[str, Any] = Field(
        ...)
    descriptors: List[Descriptor] = Field(
        ...)
    usages: Optional[List[str]] = Field(
        None)
    domain: Optional[str] = Field(
        None)
    idioms: Optional[List[str]] = Field(
        None)
    phrasalverbs: Optional[List[str]] = Field(
        None)

    def _generate(self, origin):
        origin["type"] = self.type
        origin["idioms"] = self.idioms
        origin["phrasalverbs"] = self.phrasalverbs
        origin["pronunciations"] = self.pronunciations
        origin["__domain"] = self.domain if self.domain else None
        origin["__usages"] = self.usages if self.usages else list()
        for descriptor in self.descriptors:
            yield from descriptor._generate(origin)


class Sources(BaseModel):
    cambridge_learner: Optional[List[Entry]] = Field(
        None)
    cambridge_american: Optional[List[Entry]] = Field(
        None)
    cambridge_business: Optional[List[Entry]] = Field(
        None)

    def _generate(self, origin):
        for k, v in self.__dict__.items():
            origin["source"] = k
            v = v or []
            for entry in v:
                yield from entry._generate(origin)


class Entity(BaseModel):
    hint: Optional[str] = Field(
        None)
    word: Optional[str] = Field(
        None)
    sources: Sources = Field(
        ...)

    def _generate(self) -> Generator:
        origin = {"word": self.word}
        yield from self.sources._generate(origin)

    def generate_words(self) -> Generator:
        return self._generate()
