from typing import Any, Dict, List, Optional

from pydantic import Field

from shinka.wordshard.models.base import BaseModel


class WordShard(BaseModel):
    word: str = Field(
        ...)
    source: str = Field(
        ...)
    # The type can only be omitted in these following cases:
    # A description showing different tenses of its original word
    # A description showing different forms of its original word (shed)
    type: str = Field(
        None)
    pronunciations: Optional[dict] = Field(
        ...)
    definition: str = Field(
        ...)
    examples: Optional[List[str]] = Field(
        None)
    more_examples: Optional[List[str]] = Field(
        None)
    phrase: Optional[str] = Field(
        None)
    attrs: Optional[Dict[str, Any]] = Field(
        None)
    rels: Optional[Dict[str, Any]] = Field(
        None)
    related_topics: Optional[List[dict]] = Field(
        None)
    idioms: Optional[List[str]] = Field(
        None)
    phrasalverbs: Optional[List[str]] = Field(
        None)
    guideword: Optional[str] = Field(
        None)
