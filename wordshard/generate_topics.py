import json
import pickle


with open("shards.pickle", "rb") as f:
    shards = pickle.load(f)


def generate_topics(shards):
    related_topics = []
    for shard in shards:
        rltps = shard.get("related_topics")
        if rltps:
            related_topics += rltps

    return {v["url"]: v for v in related_topics}.values()


topics = generate_topics(shards)
sorted_topics = sorted(topics, key=lambda i: i["url"])

with open("topics.json", "w+") as f:
    json.dump(sorted_topics, f, indent=2, ensure_ascii=False)
