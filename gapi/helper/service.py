from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import json
import os
from shinka.utils.common import njoin
from shinka.utils.storage import save_to_pickle, load_from_pickle


RESDIR = njoin(os.path.dirname(__file__), '../res')
CREDS = njoin(RESDIR, 'credentials.json')
TOKEN = njoin(RESDIR, 'token.pickle')
__creds = None


with open(njoin(RESDIR, 'scopes.json'), 'r', encoding='utf8') as f:
    SCOPES = json.load(f)


def retrieve_creds(src="client_secrets"):
    global __creds
    if not __creds:
        creds = load_from_pickle(TOKEN)
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    CREDS, SCOPES)
                creds = flow.run_local_server(port=0)
            save_to_pickle(TOKEN, creds)
        __creds = creds
    return __creds


def build_service(service_name, service_version):
    """Create and build a service object, according to
    the Google Documentation.
    """
    creds = retrieve_creds()
    # cache_discovery here to get rid of the nasty error
    # when importing from other modules
    # The exception won't show unless you init a logging logger
    return build(
        service_name,
        service_version,
        credentials=creds,
        cache_discovery=False
    )
