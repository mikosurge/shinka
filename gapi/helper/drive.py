import io
import json
import os
import shutil
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload
from shinka.utils.common import init_logger, njoin, stringify, truthy_dict
from shinka.utils.generator import md5


logger = init_logger(__name__)
__drive_service = None


def service():
    global __drive_service
    if not __drive_service:
        try:
            from shinka.gapi.helper.service import build_service
            __drive_service = build_service('drive', 'v3')
        except Exception as e:
            logger.error(f"Can not initialize the drive service due to {e}.")
            raise
    return __drive_service


def init_service():
    service()


def __is_downloadable(mimetype):
    if mimetype.startswith('application/vnd.google-apps'):
        return False
    else:
        return True


def __isremotedir(mimetype_or_obj):
    if isinstance(mimetype_or_obj, str):
        return mimetype_or_obj == 'application/vnd.google-apps.folder'
    elif isinstance(mimetype_or_obj, dict):
        return mimetype_or_obj.get('mimeType') \
            == 'application/vnd.google-apps.folder'


def create_empty_file(path, name):
    full_path = njoin(path, name)
    if os.path.exists(full_path):
        os.remove(full_path)
    open(full_path, 'wb').close()


def try_to_create_folder(path, keep_old=True):
    if os.path.exists(path):
        if keep_old:
            logger.warning(f"{path} already exists!")
            return

        def handle_exc(func, path, exc_info):
            logger.error(f"An error occurred when trying to delete {path}")
            logger.error(exc_info)

        logger.warning(f"Removing {path} recursively...")
        shutil.rmtree(path, onerror=handle_exc)

    logger.info(f"Creating {path}...")
    os.makedirs(path)


def list_folder(folder_id, fields=None, pageSize=100):
    if not fields:
        fields = " nextPageToken, \
files(kind, id, name, mimeType, size, md5Checksum)"
    else:
        fields = f"nextPageToken, files({', '.join(fields)})"
    items = list()
    try:
        result = service().files().list(
            pageSize=pageSize,
            q=f"'{folder_id}' in parents",
            fields=fields).execute()
        items = result.get('files')
        logger.debug(stringify(items, indent=2))
    except HttpError as err:
        logger.error(err)
    return items


def __download_file_(filedict, path, options=dict()):
    id_ = filedict.get('id')
    name = filedict.get('name')
    mimetype = filedict.get('mimeType')
    size = filedict.get('size')
    caption = f"{name}, ({id_}, {mimetype})"
    remote_md5 = filedict.get('md5Checksum')
    logger.info(f"Trying to download file {caption}")
    full_path = njoin(path, name)

    if int(size) == 0:
        logger.warning(f"[{name}] has no content.")
        create_empty_file(path, name)
    elif os.path.isfile(full_path):
        if not options.get('accept_existing'):
            local_md5 = md5(full_path)
            if local_md5 == remote_md5:
                logger.info(f"File {name} exists with the same checksum")
            else:
                logger.info(f"Local file {name} is corrupted, downloading...")
                download_file(id_, path, name)
    else:
        download_file(id_, path, name)


def download_file(file_id, local_path='', local_name='', options=dict()):
    if not local_name:
        logger.warning(f"No name specified for {file_id}, \
fetching metadata...")
        metadata = get_metadata(file_id)
        if not metadata or not metadata.get('name'):
            raise ValueError("File name not specified.")
        local_name = metadata.get('name')

    request = service().files().get_media(fileId=file_id)
    full_path = njoin(local_path, local_name)
    file_handler = io.FileIO(full_path, 'wb')
    downloader = MediaIoBaseDownload(file_handler, request)
    done = False
    while not done:
        try:
            status, done = downloader.next_chunk()
            logger.debug(
                f"[{int(status.progress() * 100)}%] Downloading {full_path}")
        except HttpError as e:
            logger.error(f"Could not download [{file_id}] due to {e}")
            return None


def download_folder(folder_id: str,
                    local_path: str = '',
                    local_name: str = None,
                    options: dict = None):
    # Options might have overwrite, accept_existing, by_name
    options = options or dict()
    if local_name is None:
        logger.warning(
            f"No name specified for {folder_id}, fetching metadata...")
        metadata = get_metadata(folder_id)
        if not metadata or not metadata.get('name'):
            raise ValueError("Folder name not specified.")
        local_name = metadata.get('name')

    full_path = njoin(local_path, local_name)
    logger.info(f"Trying to download folder {local_name}")
    try_to_create_folder(full_path)

    items = list_folder(folder_id)
    done = 0
    total = len(items)
    for item in items:
        name = item.get('name')
        mimetype = item.get('mimeType')
        if __isremotedir(mimetype):
            id_ = item.get('id')
            download_folder(id_, full_path, name, options=options)
        elif __is_downloadable(mimetype):
            __download_file_(item, full_path, options=options)
        else:
            logger.warning(f"[{name}] Not downloadable: {mimetype}")
        done += 1
        logger.info(f"[{full_path}] Completed {done}/{total} items")


def get_metadata(id_, fields=None):
    try:
        return service().files().get(fileId=id_, fields=fields).execute()
    except HttpError as e:
        logger.error(e)
        return None


def dump_metadata(path, id_, fields='*'):
    with open(path, 'w+', encoding='utf8') as f:
        json.dump(
            get_metadata(id_, fields=fields),
            f, indent=2,
            ensure_ascii=False
        )


def __diff(side, full_path, type_=None, comment=None):
    return truthy_dict({
        'side': side,
        'full_path': full_path,
        'type': type_,
        'comment': comment
    })


def compare_files(local_path, remote_dict):
    """Compare a local file from its local path with a remote file from
    its remote dict (containing its information).
    """
    local_md5 = md5(local_path)
    remote_md5 = remote_dict.get('md5Checksum')
    return local_md5 == remote_md5


def __compare_folders(local_path, remote_id, remote_path=''):
    logger.warning(f"Checking <{local_path}> and <{remote_id}>...")
    if not os.path.exists(local_path):
        raise IOError("Local path doesn't exist.")

    if not (local_children := os.listdir(local_path)):
        raise IOError("Local path is empty.")

    if (remote_children := list_folder(remote_id)) is None:
        raise IOError("Remote folder doesn't exist.")

    # TODO Handle the case when remote drive has folders with the same name
    # That leads to different children numbers but the comparison
    # might still returns True

    rnamedict = {item.get('name'): item for item in remote_children}
    for item in local_children:
        child_local_path = njoin(local_path, item)
        child_remote_path = njoin(remote_path, item)
        if os.path.isdir(child_local_path):
            if item in rnamedict:
                if __isremotedir(rnamedict[item]):
                    yield from __compare_folders(
                        child_local_path,
                        rnamedict[item].get('id'),
                        child_remote_path)
                else:
                    yield __diff('both', child_local_path)
            else:
                yield __diff('local', child_local_path, 'folder')
        elif os.path.isfile(child_local_path):
            if item in rnamedict:
                if __isremotedir(rnamedict[item]):
                    yield __diff('both', child_local_path)
                elif not compare_files(child_local_path, rnamedict[item]):
                    yield __diff('both', child_local_path, 'file')
            else:
                yield __diff('local', child_local_path, 'file')

    for item in rnamedict:
        if item not in local_children:
            child_remote_path = njoin(remote_path, item)
            yield __diff('remote', child_remote_path)


def compare_folders(local_path, remote_id):
    """Compare a local folder with its remote counterpart id on drive.
    """
    return list(__compare_folders(local_path, remote_id)) or list()
