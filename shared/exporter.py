from shinka.utils.common import init_logger


logger = init_logger(__name__)


class PlainExporter:
    @classmethod
    def export(cls, dst, content):
        logger.info(f"Writing content to file {dst}")
        with open(dst, 'w+', encoding='utf8') as f:
            f.write(content)
