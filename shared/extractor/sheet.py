import json
import gspread

from gapi.helper.service import retrieve_creds


creds = retrieve_creds()
gc = gspread.authorize(creds)


def export_sheet(workbook, worksheet, outpath):
    _ws = gc.open(workbook).worksheet(worksheet)
    with open(outpath, 'w+', encoding='utf8') as f:
        json.dump(_ws.get_all_values(), f, indent=2, ensure_ascii=False)
