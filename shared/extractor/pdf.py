import PyPDF2
from pdfminer.high_level import extract_text


"""
This extractor uses PyPDF2 and pdfminer for extracting purpose.
Due to a case of stranger characters retrieved when using PyPDF2,
pdfminer is added as the second method.
"""


def flatten_page_list(pages):
    """Flatten every gap in a given page list.

    Examples:
        >>> flatten_page_list([1, (3, 6), 12])
        [1, 3, 4, 5, 6, 12]
    """
    if pages is None:
        return None
    list_ = list()
    for page in pages:
        if isinstance(page, tuple):
            list_ += range(page[0] - 1, page[1])
        else:
            list_.append(page - 1)
    return list_


def pypdf_strat(path, pages=None):
    pdf_reader = PyPDF2.PdfFileReader(open(path, 'rb'))
    if not pages:
        pages = range(pdf_reader.getNumPages())

    text = ''
    for page in pages:
        try:
            pageobj = pdf_reader.getPage(page)
            text += pageobj.extractText()
            text += "\n"
        except IndexError:
            pass
    return text


def pdfminer_strat(path, pages):
    return extract_text(path, page_numbers=pages)


class PdfExtract:
    def __init__(self, path, strat='pdfminer'):
        self._path = path
        if strat == 'pypdf2':
            self.strat = pypdf_strat
        elif strat == 'pdfminer':
            self.strat = pdfminer_strat
        else:
            raise ValueError("Unrecognized PDF extracting strategy.")

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def strat(self):
        return self._strat

    @strat.setter
    def strat(self, value):
        self._strat = value

    def extract(self, dst=None, pages=None):
        text = self.strat(self.path, flatten_page_list(pages))
        if dst:
            with open(dst, 'w+', encoding='utf8') as f:
                f.write(text)
        return text
