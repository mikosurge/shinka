import requests
from shinka.utils.common import init_logger


logger = init_logger(__name__)


class IFetcher:
    def fetch(cls, url, *args, **kwargs):
        pass


class SimpleHTTPFetcher(IFetcher):
    @classmethod
    def fetch(cls, url, *args, **kwargs):
        data = None
        try:
            logger.debug(f"Fetching data from {url}")
            session = requests.Session()
            session.trust_env = False
            response = session.get(url, *args, **kwargs)
            data = response.content.decode()
        except OSError as oe:
            logger.warning(oe)
            raise
        except Exception as e:
            logger.warning(e)
            raise
        return data
