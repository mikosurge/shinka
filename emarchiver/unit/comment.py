from shinka.utils.common import stringify, truthy_dict


class Comment:
    def __init__(
        self,
        id_: str,
        no: int,
        game_id: str,
        time: dict = None,  # contains timestamp and datetime
        user: dict = None,  # contains avatar, name, and role
        content: dict = None,
        replies: list = None,
        reactions: dict = None,
        edit_history: list = None,
        editted: bool = False,
        type_: str = None,
        origin: str = None,
        day: int = 0
    ):
        self.id_ = id_
        self.no = no
        self.game_id = game_id
        self.time = time
        self.user = user
        self.content = content or dict()
        self.replies = replies or list()
        self.reactions = reactions or dict()
        self.edit_history = edit_history or list()
        self.editted = editted
        self.type_ = type_
        self.origin = origin
        self.day = day

    @classmethod
    def from_dict(cls, dict_):
        id_ = dict_.get('id')
        no = dict_.get('no')
        game_id = dict_.get('game_id')
        time = dict_.get('time', dict())
        user = dict_.get('user', dict())
        content = dict_.get('content', dict())
        replies = dict_.get('replies', list())
        reactions = dict_.get('reactions', dict())
        edit_history = dict_.get('edit_history', list())
        editted = dict_.get('editted')
        type_ = dict_.get('type_')
        origin = dict_.get('origin')
        day = dict_.get('day')
        return cls(id_, no, game_id, time, user,
                   content, replies, reactions, edit_history,
                   editted, type_, origin, day)

    def serialize(self):
        return truthy_dict({
            'id': self.id_,
            'no': self.no,
            'game_id': self.game_id,
            'time': self.time,
            'user': self.user,
            'content': self.content,
            'replies': [
                cm.serialize()
                for cm in self.replies
            ],
            'reactions': self.reactions,
            'edit_history': self.edit_history,
            'editted': self.editted,
            'type': self.type_,
            'origin': self.origin,
            'day': self.day
        })

    def stringify(self):
        return stringify(self.serialize())
