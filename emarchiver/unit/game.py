from shinka.utils.common import stringify, truthy_dict


class Game:
    def __init__(
        self,
        id_: str = None,
        name: str = None,
        host: str = None,
        duration: int = None,
        where: str = None,
        comments: list = None,
    ):
        self.id_ = id_
        self.name = name
        self.host = host
        self.duration = duration
        self.where = where
        self.comments = comments or list()

    @classmethod
    def from_dict(cls, dict_):
        id_ = dict_.get('id')
        name = dict_.get('name')
        host = dict_.get('host')
        duration = dict_.get('duration', 0)
        where = dict_.get('where')
        comments = dict_.get('comments', list())
        return cls(id_, name, host,
                   duration, where, comments)

    def serialize(self):
        comments = [
            cm.serialize()
            for cm in self.comments
        ]
        dict_ = {
            "id": self.id_,
            "name": self.name,
            "host": self.host,
            "duration": self.duration,
            "where": self.where,
            "comments": comments
        }
        return truthy_dict(dict_)

    def stringify(self):
        return stringify(self.serialize())
