from bs4 import BeautifulSoup
import json
import re
import requests
from shinka.emarchiver.parser.forumvi import ForumViGameParser
from shinka.utils.common import init_logger


logger = init_logger(__name__)
daydict = {
    1: "https://teaparty.forumvi.com/t10-topic"
}


def count_vote(game):
    vote_rgx = r'^.*\s*#VOTE: ([A-Za-z]+)\s*.*$'
    novote_rgx = r'^.*\s*#NOVOTE\s*.*$'
    unvote_rgx = r'^.*\s*#UNVOTE\s*.*$'
    regexes = [vote_rgx, novote_rgx, unvote_rgx]
    commands = ["VOTE", "NOVOTE", "UNVOTE"]
    votes = list()
    for cmobj in game.comments:
        if cmobj.no == 1:
            continue
        bolds = cmobj.content['exclusive'].split('\n')
        for bold in bolds:
            for i, regex in enumerate(regexes):
                if ptn := re.search(regex, bold):
                    sub = ptn.group(1) if len(ptn.groups()) else ''
                    logger.info(f"{cmobj.user['name']} {commands[i]} {sub} \
at comment {cmobj.no}")
                    votes.append(
                        (cmobj.user['name'], commands[i], sub, cmobj.no))
    return votes


def count_posts(game):
    timergx = r'([0-9]?[0-9]:[0-9][0-9] [a|p]m)'
    cmdict = dict()
    timedict = dict()
    for cmobj in game.comments:
        if cmobj.no == 1:
            continue
        username = cmobj.user['name']
        if not cmdict.get(username):
            cmdict[username] = list()
        if not timedict.get(username):
            timedict[username] = list()
        cmdict[username].append(cmobj.no)
        ptn = re.search(timergx, cmobj.time['datetime'])
        if ptn and len(ptn.groups()):
            timedict[username].append(ptn.group(1))
        else:
            logger.info(cmobj.time['datetime'])

    for user, posts in cmdict.items():
        logger.info(f"{user} has {len(posts)} posts: {posts}")

    with open('timedict.json', 'w+', encoding='utf8') as f:
        json.dump(timedict, f, indent=2)
    for user, times in timedict.items():
        logger.info(f"{user} appeared at {' - '.join(times)}")


def generate_votemaps(vote_tuples):
    votemap = dict()
    for vote in vote_tuples:
        votemap[vote[0]] = {
            'vote': None,
            'num': 0,
            'previous': list()
        }
    for vote in vote_tuples:
        player = vote[0]
        if votemap[player]['num'] >= 2:
            logger.warning(f"LIMIT EXCEEDED: {vote}")

        cmd = vote[1]
        if cmd == 'VOTE' or cmd == 'NOVOTE':
            if not votemap[player]['vote']:
                target = vote[2] if cmd == 'VOTE' else 'NOVOTE'
                votemap[player]['vote'] = target
                votemap[player]['num'] += 1
            else:
                logger.warning(f"INVALID command: {vote}")
        elif cmd == 'UNVOTE':
            if votemap[player]['vote']:
                votemap[player]['previous'].append(votemap[player]['vote'])
                votemap[player]['vote'] = ''
            else:
                logger.warning(f"INVALID command: {vote}")
    targetmap = dict()
    for player, pmap in votemap.items():
        target = pmap['vote']
        if not targetmap.get(target):
            targetmap[target] = list()
        targetmap[target].append(player)
    logger.info(votemap)
    logger.info(targetmap)
    return (votemap, targetmap)


fgp = ForumViGameParser(daydict)
game = fgp.parse()
fgp.export_all_htmls()


votes = count_vote(game)
votemap, targetmap = generate_votemaps(votes)
count_posts(game)


with open('forumvi.json', 'w+', encoding='utf8') as f:
    f.write(game.stringify())
