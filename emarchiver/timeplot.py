import matplotlib.pyplot as plt
import json
import numpy as np
import os


with open('timedict.json', 'r', encoding='utf8') as f:
    timedict = json.load(f)

if not os.path.exists('./graph'):
    os.makedirs('./graph')


def generate_graph(name, timestamps):
    time24 = list()
    for dt in timestamps:
        lr = dt.split(' ')
        hm = lr[0].split(':')
        ap = lr[1]
        hour = int(hm[0])
        mins = int(hm[1])
        if not (hour == 12 and ap == "am"):
            if ap == "pm" and hour < 12:
                time24.append((hour + 12, mins))
            elif ap == "am":
                time24.append((hour, mins))
            else:
                time24.append((hour, mins))

    ratedict = dict()
    h24 = [t[0] for t in time24]
    for tt in h24:
        if not ratedict.get(tt):
            ratedict[tt] = 1
        else:
            ratedict[tt] += 1

    x = np.arange(8, 24, 0.125)
    y = {
        i: 0
        for i in x
    }
    for h, m in time24:
        val = h + m / 60
        for i in x:
            if i <= val and val < i + 0.125:
                y[i] += 1

    yl = [
        y[k]
        for k in sorted(y.keys())
    ]

    fig = plt.figure(figsize=(10, 5))
    fig.suptitle(name)
    plt.plot(x, yl, 'bo', x, yl, 'k')
    plt.xticks(np.arange(8, 24, 1))
    plt.yticks(np.arange(0, 10, 1))
    plt.xlabel("hour")
    plt.ylabel("posts")
    plt.savefig(f"./graph/{name}.png")
    plt.clf()


for name, tss in timedict.items():
    generate_graph(name, tss)
