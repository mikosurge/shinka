import bs4
import json
import os
import re
import time

from shinka.emarchiver.parser.game import GameParser
from shinka.emarchiver.unit.game import Game
from shinka.emarchiver.unit.comment import Comment
from shinka.shared.fetcher import NoProxyHTTPFetcher
from shinka.utils.common import init_logger, njoin, safeint, \
    truthy_dict, truthy_list
from shinka.utils.soup import first, ftext, textlist
from shinka.utils.markdown import format_md, fix_escape_chars


logger = init_logger(__name__)
RESDIR = njoin(os.path.dirname(__file__), '../res')
with open(njoin(RESDIR, 'wix.json'), 'r', encoding='utf8') as f:
    RES = json.load(f)
    SELECTORS = RES['selectors']
    CLASSES = RES['classes']


def get_prev_and_next(node, current):
    """Detect the next and previous page
    """
    PAGINATION = SELECTORS['pagination']
    if not (current_page := current or ftext(node, PAGINATION['current'])):
        return None
    current_page = int(current_page)

    def get_num_and_href(pagnode):
        return (int(pagnode.get_text()), pagnode.get('href'))

    other_pages = list(
        map(get_num_and_href, node.select(PAGINATION['others'])))
    # if not other_pages:
    #     return None

    next_page = min(
        list(filter(lambda i: i[0] > current_page, other_pages)),
        default=None)
    prev_page = max(
        list(filter(lambda i: i[0] < current_page, other_pages)),
        default=None)
    return [prev_page, next_page]


def to_markdown(nodeobj):
    if not isinstance(nodeobj, dict):
        return nodeobj

    if nodeobj.get('hidden', False):
        return ''

    children = list()
    for child in nodeobj.get('children', list()):
        children.append(to_markdown(child))

    separator = '' if nodeobj.get('inline', False) else '\n'
    content = separator.join(children)
    for fmt in nodeobj.get('format', list()):
        content = format_md(content, fmt, nodeobj.get('sub', ''))
    return content


def update_attrs(childobj, node, num):
    """Add all information required for a node object so that it can be
    used to retrieve the corresponding text format.
    """
    tag = node.name
    cls = node.get('class')
    fmt = list()
    sub = None
    hidden = False
    inline = True

    if tag == 'p':
        pass
    elif tag == 'a':
        if sub := node.get('href', ''):
            fmt.append('link')
    elif tag == 'blockquote':
        fmt.append('blockquote')
    elif tag == 'div':
        inline = False
    elif tag == 'img':
        if CLASSES['tag']['image1'] in cls:
            sub = node.get('src')
            fmt.append('image')
        else:
            hidden = True
    elif tag == 'ul':
        fmt.append('ul')
        inline = False
        for child in childobj.get('children', list()):
            if child['tag'] == 'li':
                child['format'].append('li-unordered')
    elif tag == 'ol':
        fmt.append('ol')
        inline = False
        for child in childobj.get('children', list()):
            if child['tag'] == 'li':
                child['format'].append('li-ordered')
    elif tag == 'li':
        fmt.append('li')
        sub = num + 1
    elif tag == 'span':
        if style := node.get('style'):
            if pattern := re.search(r"^(color:(.*))$", style):
                fmt.append('color')
                sub = pattern.group(1)
    elif tag == 'strong':
        fmt.append('bold')
    elif tag == 'em':
        fmt.append('italics')
    elif tag == 'article':
        inline = False
    elif tag == 'path':
        hidden = True
    elif tag == 'svg':
        pass
    elif tag == 'button':
        hidden = True
    elif tag == 'br':
        fmt.append('line-break')
    elif tag == 'hr':
        fmt.append('horizontal-rule')
    elif tag == 'u':
        fmt.append('underline')
    elif tag == 'h1':
        fmt.append(tag)
    elif tag == 'h2':
        fmt.append(tag)
    elif tag == 'h3':
        fmt.append(tag)
    elif tag == 'line':
        fmt.append('horizontal-rule')
    elif tag == 'pre':
        fmt.append('codeblock')
    else:
        logger.error(f"Undefined tag: {tag}")

    if cls:
        for k, v in CLASSES['format'].items():
            if v in cls:
                fmt.append(k)

    childobj.update(truthy_dict({
        "tag": tag,
        "format": fmt,
        "inline": inline,
        "hidden": hidden,
        "classes": cls,
        "sub": sub
    }))


def get_meta(node, is_pinned_post):
    COMPONENTS = SELECTORS['component']
    namesel = COMPONENTS['username-pinned'] \
        if is_pinned_post else COMPONENTS['username']
    username = ftext(node, namesel)
    role = ftext(node, COMPONENTS['role'])
    datetime = ftext(node, COMPONENTS['datetime'])
    total_reactions = safeint(ftext(node, COMPONENTS['reactions']))
    editted = ftext(node, COMPONENTS['editted'])
    return {
        'user': {
            'name': username,
            'role': role,
        },
        'time': {
            'datetime': datetime,
        },
        'reactions': {
            'total': total_reactions,
        },
        'editted': editted,
    }


def parse_content(node, num=0):
    if isinstance(node, bs4.element.NavigableString):
        result = fix_escape_chars(str(node))
    elif isinstance(node, bs4.element.Comment):
        result = ''
    elif isinstance(node, bs4.element.Tag):
        result = dict()
        result['children'] = list()
        for i, content in enumerate(node.contents):
            result['children'].append(parse_content(content, i))
        update_attrs(result, node, num)
    else:
        raise ValueError(f"Unknown node type of {type(node)}")
    return result


def extract_comment(node, type_, numdict=None):
    """Extracts a comment from a soup object via a selector
    of its corresponding wrapper (article).

    Args:
        type_: Either 'pinned', 'comment' or 'reply'
    """
    COMPONENTS = SELECTORS['component']
    inner = node.select_one(COMPONENTS['inner'])
    content = parse_content(inner)
    cmdict = dict()
    id_ = node.get('id')
    if type_ == 'pinned':
        if more := first(node, COMPONENTS['btn-more']):
            btn_id = more.get('id')
            if pattern := re.search(r"^more-button-([a-z0-9]+)", btn_id):
                id_ = pattern.group(1)

    cmdict.update({
        'id': id_,
        'content': to_markdown(content),
        'type': type_,
    })
    cmdict.update(get_meta(node, type_ == 'pinned'))
    if type_ != 'reply':
        cmdict.update({
            'day': numdict['day'],
            'no': numdict['post'],
        })
        numdict['post'] += 1
    return Comment.from_dict(cmdict)


class WixGameParser(GameParser):
    def __init__(self, days):
        super().__init__(days)

    def get_relatable_resources(self):
        PAGINATION = SELECTORS['pagination']

        def tryload(link):
            html = NoProxyHTTPFetcher.fetch(link)
            soup = bs4.BeautifulSoup(html, 'html.parser')
            return [html, soup]

        def safeload(link, validator, *args):
            html, soup = tryload(link)
            while not (retval := validator(soup, *args)):
                logger.error(f"Validating {link} failed, retrying...")
                html, soup = tryload(link)
            return (retval, html, soup)

        def validator_current(soup, sel):
            """Detects the current page and return it as retval.
            """
            return ftext(soup, sel)

        def validator_prevnext(soup, pageno):
            return get_prev_and_next(soup, pageno)

        for no, link in self.days.items():
            pagedict = dict()
            retval, html, soup = safeload(
                link, ftext, PAGINATION['current'])
            pagedict[int(retval)] = (link, html, soup)

            prev_page, next_page = get_prev_and_next(soup, retval)
            while next_page:
                logger.info(f"Fetching {next_page}")
                retval, html, soup = safeload(
                    next_page[1], get_prev_and_next, next_page[0])
                pagedict[next_page[0]] = (next_page[1], html, soup)
                next_page = retval[1]

            while prev_page:
                logger.info(f"Fetching {prev_page}")
                retval, html, soup = safeload(
                    prev_page[1], get_prev_and_next, prev_page[0])
                pagedict[prev_page[0]] = (prev_page[1], html, soup)
                prev_page = retval[0]

            self.pagedicts.append(pagedict)

    def parse_gameinfo(self):
        pass

    def parse_page(self, soup, numdict: dict):
        """
        Args:
            soup: The soup node.
            numdict: The constantly updating dict for numbering.

        """
        POST = SELECTORS['post']
        logger.info(f"Parsing page {numdict['page']}")
        comments = list()
        if pinned := soup.select_one(POST['pinned']):
            comments.append(extract_comment(pinned, 'pinned', numdict))

        for i, cm in enumerate(soup.select(POST['comment-wrapper'])):
            cmnode = cm.select_one(POST['comment'])
            comment = extract_comment(cmnode, 'comment', numdict)

            comment.replies = [
                extract_comment(rpnode, 'reply')
                for rpnode in cm.select(POST['reply'])
            ]
            comments.append(comment)
        return comments

    def parse_all_days(self):
        comments = list()
        for dayno, pagedict in enumerate(self.pagedicts):
            logger.info(f"Parsing day {dayno + 1}")
            pagesoups = [pagedict[key][2] for key in sorted(pagedict.keys())]
            numdict = {
                'day': dayno + 1,
                'post': 1
            }
            for pageno, pagesoup in enumerate(pagesoups):
                numdict['page'] = pageno + 1
                comments += self.parse_page(pagesoup, numdict)
        self.game.comments = comments
