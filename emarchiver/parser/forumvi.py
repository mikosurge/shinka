import bs4
import json
import os
import re

from shinka.emarchiver.parser.game import GameParser
from shinka.emarchiver.unit.game import Game
from shinka.emarchiver.unit.comment import Comment
from shinka.shared.fetcher import SimpleHTTPFetcher
from shinka.utils.common import init_logger, njoin, truthy_dict, truthy_list
from shinka.utils.soup import first, ftext, textlist
from shinka.utils.markdown import fix_escape_chars


DOMAIN = 'https://teaparty.forumvi.com/'
logger = init_logger(__name__)
RESDIR = njoin(os.path.dirname(__file__), '../res')
TMPDIR = njoin(os.path.dirname(__file__), '../temp')
with open(njoin(RESDIR, 'forumvi.json'), 'r', encoding='utf8') as f:
    RES = json.load(f)
    SELECTORS = RES['selectors']
    CLASSES = RES['classes']
    HEADERS = RES['headers']
    REGEXES = RES['regex']


formatdict = {
    "bold": "**{content}**",
    "italics": "*{content}*",
    "underline": "__{content}__",
    "strikethrough": "~~{content}~~",
    "blockquote": "> {content}",
    "ul": "{content}\n",
    "ol": "{content}\n",
    "li": "{content}",
    "li-unordered": "- {content}",
    "li-ordered": "{sub}. {content}",
    "link": "[{content}]({sub})",
    "h1": "# {content}",
    "h2": "## {content}",
    "h3": "### {content}",
    "codeblock-inline": "`{content}`",
    "codeblock": "```\n{content}\n```",
    "line-break": "",
    "horizontal-rule": "___",
    "hidden": "",
    "color": "<span style=\"{sub}\">{content}</span>",
    "image": "![{content}]({sub})",
    "center": "<center>{content}</center>",
    "emoticon": "![{content}]({sub})",
    "quote": "> [{content}]({sub})\n",
    "spoiler": "||{content}||",
}


def format_md(content, type_, sub):
    if formatdict.get(type_):
        raw = content.strip(' ')
        if raw or type_ == 'horizontal-rule':
            leftspaces = len(content) - len(content.lstrip(' '))
            rightspaces = len(content) - len(content.rstrip(' '))
            return ' ' * leftspaces + \
                formatdict[type_].format(content=raw, sub=sub) + \
                ' ' * rightspaces
    return content


def recover(path, domain=DOMAIN):
    if not path.startswith('http'):
        return f"{domain}{path}"
    return path


def get_prev_and_next(node, current=None):
    PAG = SELECTORS['pagination']
    pagnode = node.select_one(PAG['main'])
    if not (current_page := current or ftext(pagnode, PAG['current'])):
        return None
    current_page = int(current_page)

    def get_num_and_href(pagnode):
        return (int(pagnode.get_text()), recover(pagnode.get('href')))

    # TODO Change wix.py if the expression below succeed
    if not (other_pages := list(
            map(get_num_and_href, pagnode.select(PAG['others'])))):
        return None

    next_page = min(
        list(filter(lambda i: i[0] > current_page, other_pages)),
        default=None)
    prev_page = max(
        list(filter(lambda i: i[0] < current_page, other_pages)),
        default=None)
    return [prev_page, next_page]


def update_attrs(childobj, node, num):
    """Add all information required for a node object so that it can be
    used to retrieve the corresponding text format.
    """
    tag = node.name
    cls = node.get('class')
    fmt = list()
    sub = None
    hidden = False
    inline = True

    if tag == 'p':
        pass
    elif tag == 'a':
        if sub := recover(node.get('href', '')):
            fmt.append('link')
    elif tag == 'blockquote':
        # Worwor exclusive
        if first(node, 'span[title="Source of the post"]'):
            sub = recover(node.select_one(
                    'a[href^="./viewtopic.php"]').get('href'))
            childobj.update({
                'text': ftext(node, 'cite'),
                'href': sub,
            })
            fmt.append('quote')
        else:
            fmt.append('blockquote')
    elif tag == 'div':
        pass
    elif tag == 'img':
        sub = recover(node.get('src'))
        # Worwor exclusive
        fmt.append('image')
    elif tag == 'ul':
        fmt.append('ul')
        inline = False
        for child in childobj.get('children', list()):
            if child['tag'] == 'li':
                child['format'].append('li-unordered')
    elif tag == 'ol':
        fmt.append('ol')
        inline = False
        for child in childobj.get('children', list()):
            if child['tag'] == 'li':
                child['format'].append('li-ordered')
    elif tag == 'li':
        fmt.append('li')
        sub = num + 1
    elif tag == 'span':
        # TODO Check if there is any valuable attributes
        pass
    elif tag == 'strong':
        fmt.append('bold')
    elif tag == 'em':
        fmt.append('italics')
    elif tag == 'article':
        inline = False
    elif tag == 'path':
        hidden = True
    elif tag == 'svg':
        pass
    elif tag == 'button':
        hidden = True
    elif tag == 'br':
        fmt.append('line-break')
    elif tag == 'hr':
        fmt.append('horizontal-rule')
    elif tag == 'u':
        fmt.append('underline')
    elif tag == 'h1':
        fmt.append(tag)
    elif tag == 'h2':
        fmt.append(tag)
    elif tag == 'h3':
        fmt.append(tag)
    elif tag == 'line':
        fmt.append('horizontal-rule')
    elif tag == 'pre':
        fmt.append('codeblock')
    # Worwor exclusive
    elif tag == 'center':
        fmt.append('center')
    elif tag == 'cite':
        fmt.append('cite')
    elif tag == 'code':
        fmt.append('codeblock')
    elif tag == 'strike':
        fmt.append('strikethrough')
    # Forumvi exclusive
    elif tag == 'dl':
        # Codeblocks have class of "codebox"
        pass
    elif tag == 'dt':
        pass
    elif tag == 'dd':
        pass
    elif tag == 'font':
        # Text color is embedded in this tag
        pass
    elif tag == 'i':
        fmt.append('italics')
    else:
        logger.error(f"Undefined tag: {tag}")

    if cls:
        for k, v in CLASSES['format'].items():
            if v in cls:
                fmt.append(k)

    childobj.update(truthy_dict({
        "tag": tag,
        "format": fmt,
        "inline": inline,
        "hidden": hidden,
        "classes": cls,
        "sub": sub
    }))


def to_markdown(nodeobj):
    if not isinstance(nodeobj, dict):
        return nodeobj

    if nodeobj.get('hidden', False):
        return ''

    children = list()
    for child in nodeobj.get('children', list()):
        children.append(to_markdown(child))

    separator = '' if nodeobj.get('inline', False) else '\n'
    content = separator.join(children)
    for fmt in nodeobj.get('format', list()):
        # TODO Change to format every separated by '\n' elements
        if fmt == 'emoticon':
            content = f"{nodeobj.get('alt')} ({nodeobj.get('title')})"
        elif fmt == 'quote':
            expr = re.compile(REGEXES['post-id'])
            pattern = re.search(expr, nodeobj.get('href'))
            id_ = pattern.group(1) or ''
            content = f"{nodeobj.get('text')} <Post #{id_}>"

        if nodeobj.get('inline'):
            lines = content.split('\n')
            content = '\n'.join([
                format_md(line, fmt, nodeobj.get('sub', ''))
                for line in lines
            ])
        else:
            content = format_md(content, fmt, nodeobj.get('sub', ''))
    return content


def get_exclusive(nodeobj, exclfn):
    if not isinstance(nodeobj, dict):
        return (nodeobj, '')

    if nodeobj.get('hidden', False) or nodeobj.get('tag') == 'blockquote':
        return ('', '')

    if 'line-break' in nodeobj.get('format', list()):
        return ('\n', '\n')

    exclusive = exclfn(nodeobj)
    children = list()
    for child in nodeobj.get('children', list()):
        children.append(get_exclusive(child, exclfn))

    separator = '' if nodeobj.get('inline', False) else '\n'
    full_content = separator.join([child[0] for child in children])
    excl_content = full_content if exclusive else separator.join(
        [child[1] for child in children])
    return (full_content, excl_content)


def to_plaintext(nodeobj, markdown=False):
    if not isinstance(nodeobj, dict):
        return nodeobj

    if nodeobj.get('hidden', False):
        return ''

    children = list()
    for child in nodeobj.get('children', list()):
        children.append(to_plaintext(child, markdown))

    separator = '' if nodeobj.get('inline', False) else '\n'
    content = separator.join(children)
    for fmt in nodeobj.get('format', list()):
        if markdown:
            if nodeobj.get('inline'):
                lines = content.split('\n')
                content = '\n'.join([
                    format_md(line, fmt, nodeobj.get('sub', ''))
                    for line in lines
                ])
            else:
                content = format_md(content, fmt, nodeobj.get('sub', ''))

        # TODO Change to format every separated by '\n' elements
        if fmt == 'emoticon':
            if markdown:
                content = f"{nodeobj.get('alt')} ({nodeobj.get('title')})"
            else:
                content = nodeobj.get('alt')
        elif fmt == 'quote':
            expr = re.compile(REGEXES['post-id'])
            pattern = re.search(expr, nodeobj.get('href'))
            id_ = pattern.group(1) or ''
            content = f"{nodeobj.get('text')} <Post #{id_}>"
        elif fmt == 'line-break':
            content = '\n'

    return content


def get_meta(node):
    COMPONENTS = SELECTORS['component']
    username = ftext(node, COMPONENTS['username'])
    avatar = None
    if avatarnode := first(node, COMPONENTS['avatar']):
        avatar = recover(avatarnode.get('src'))
    author = first(node, COMPONENTS['datetime'])
    datetime = author.contents[-1]
    reactions = node.select(COMPONENTS['reactions'])
    total_reactions = len(reactions)
    users_reactions = [
        r.get_text()
        for r in reactions
    ]
    editted = ftext(node, COMPONENTS['editted'])
    # linknode = first(node, COMPONENTS['link'])
    # link = linknode.get('href') if linknode else None
    return {
        'user': {
            'name': username,
            'avatar': avatar,
        },
        'time': {
            'datetime': datetime,
        },
        'reactions': truthy_dict({
            'total': total_reactions,
            'users': users_reactions,
        }),
        'editted': editted,
    }


def parse_content(node, num=0, md_escape=False):
    """
    All bs4.element.Comment are bs4.element.NavigableString,
    while the reversed thing is not correct.
    """
    if isinstance(node, bs4.element.Comment):
        result = ''
    elif isinstance(node, bs4.element.NavigableString):
        result = fix_escape_chars(str(node)) if md_escape else str(node)
    elif isinstance(node, bs4.element.Tag):
        result = dict()
        result['children'] = list()
        for i, content in enumerate(node.contents):
            result['children'].append(parse_content(content, i))
        update_attrs(result, node, num)
    else:
        raise ValueError(f"Unknown node type of {type(node)}")
    return result


def extract_comment(node, type_, numdict):
    COMPONENTS = SELECTORS['component']
    inner = node.select_one(COMPONENTS['content'])
    content_md = parse_content(inner, md_escape=True)
    content = parse_content(inner)

    cmdict = dict()
    id_ = ''.join(filter(lambda i: i.isnumeric(), node.get('id')))

    def bold_only(nodeobj):
        return True if 'bold' in nodeobj.get('format', list()) else False
    cmdict.update({
        'id': id_,
        'content': {
            'markdown': to_plaintext(content_md, markdown=True),
            'exclusive': get_exclusive(content, bold_only)[1],
            'raw': to_plaintext(content)
        },
        'type': type_,
        'day': numdict['day'],
        'no': numdict['post'],
    })
    cmdict.update(get_meta(node))
    numdict['post'] += 1
    return Comment.from_dict(cmdict)


class ForumViGameParser(GameParser):
    def __init__(self, days):
        super().__init__(days)

    def get_relatable_resources(self):
        PAG = SELECTORS['pagination']

        def tryload(link):
            html = SimpleHTTPFetcher.fetch(link, headers=HEADERS)
            soup = bs4.BeautifulSoup(html, 'html.parser')
            return [html, soup]

        def safeload(link, validator, *args):
            html, soup = tryload(link)
            while not (retval := validator(soup, *args)):
                logger.error(f"Validating {link} failed, retrying...")
                html, soup = tryload(link)
            return (retval, html, soup)

        for no, link in self.days.items():
            pagedict = dict()
            retval, html, soup = safeload(link, ftext, PAG['current'])
            pagedict[int(retval)] = (link, html, soup)

            prev_page, next_page = get_prev_and_next(soup, retval)
            while next_page:
                logger.info(f"Fetching {next_page}")
                retval, html, soup = safeload(
                    next_page[1], get_prev_and_next, next_page[0])
                pagedict[next_page[0]] = (next_page[1], html, soup)
                next_page = retval[1]

            while prev_page:
                logger.info(f"Fetching {prev_page}")
                retval, html, soup = safeload(
                    prev_page[1], get_prev_and_next, prev_page[0])
                pagedict[prev_page[0]] = (prev_page[1], html, soup)
                prev_page = retval[0]

            self.pagedicts.append(pagedict)

    def parse_gameinfo(self):
        pass

    def parse_page(self, soup, numdict: dict):
        POST = SELECTORS['post']
        logger.info(f"Parsing page {numdict['page']}")
        comments = list()
        for i, cm in enumerate(soup.select(POST['comment-wrapper'])):
            comment = extract_comment(cm, 'comment', numdict)
            comments.append(comment)
        return comments

    def parse_all_days(self):
        comments = list()
        for dayno, pagedict in enumerate(self.pagedicts):
            logger.info(f"Parsing day {dayno + 1}")
            pagesoups = [pagedict[key][2] for key in sorted(pagedict.keys())]
            numdict = {
                'day': dayno + 1,
                'post': 1
            }
            for pageno, pagesoup in enumerate(pagesoups):
                numdict['page'] = pageno + 1
                comments += self.parse_page(pagesoup, numdict)
        self.game.comments = comments

    def export_all_htmls(self):
        for dayno, pagedict in enumerate(self.pagedicts):
            for pageno, content in pagedict.items():
                name = f"day-{dayno + 1}-page-{pageno}.html"
                logger.info(f"Writing content to {name}")
                with open(njoin(TMPDIR, name), 'w+', encoding='utf8') as f:
                    f.write(content[1])
