import bs4
import json
import os
import re

from shinka.emarchiver.parser.game import GameParser
from shinka.emarchiver.unit.game import Game
from shinka.emarchiver.unit.comment import Comment
from shinka.shared.fetcher import NoProxyHTTPFetcher
from shinka.utils.common import init_logger, njoin, \
    truthy_dict, truthy_list, safeint
from shinka.utils.soup import first, ftext, textlist
from shinka.utils.markdown import fix_escape_chars


DOMAIN = 'http://vnsharing.site/forum/'
logger = init_logger(__name__)
RESDIR = njoin(os.path.dirname(__file__), '../res')
TMPDIR = njoin(os.path.dirname(__file__), '../temp')
with open(njoin(RESDIR, 'vns.json'), 'r', encoding='utf8') as f:
    RES = json.load(f)
    SELECTORS = RES['selectors']
    CLASSES = RES['classes']
    HEADERS = RES['headers']


formatdict = {
    "bold": "**{content}**",
    "italics": "*{content}*",
    "underline": "__{content}__",
    "strikethrough": "~~{content}~~",
    "blockquote": "> {content}",
    "ul": "{content}\n",
    "ol": "{content}\n",
    "li": "{content}",
    "li-unordered": "- {content}",
    "li-ordered": "{sub}. {content}",
    "link": "[{content}]({sub})",
    "h1": "# {content}",
    "h2": "## {content}",
    "h3": "### {content}",
    "codeblock-inline": "`{content}`",
    "codeblock": "```\n{content}\n```",
    "line-break": "",
    "horizontal-rule": "___",
    "hidden": "",
    "color": "<span style=\"{sub}\">{content}</span>",
    "image": "![{content}]({sub})",
    "center": "<center>{content}</center>",
    "emoticon": "![{content}]({sub})",
    "quote": "> [{content}]({sub})",
    "spoiler": "||{content}||",
}


def recover(path, domain=DOMAIN):
    if not path.startswith('http'):
        return f"{domain}{path}"
    return path


def get_prev_and_next(node, current=None):
    PAG = SELECTORS['pagination']
    pagnode = node.select_one(PAG['main'])
    if not (current_page := current or ftext(pagnode, PAG['current'])):
        return None
    current_page = int(current_page)

    def get_num_and_href(pagnode):
        # The next, prev, first, last have to be ignored
        if num := safeint(pagnode.get_text()):
            return (num, recover(pagnode.get('href')))
        return None

    if not (other_pages := truthy_list(list(
            map(get_num_and_href, pagnode.select(PAG['others']))))):
        # The next, prev, first, last have to be ignored
        return None

    next_page = min(
        list(filter(lambda i: i[0] > current_page, other_pages)),
        default=None)
    prev_page = max(
        list(filter(lambda i: i[0] < current_page, other_pages)),
        default=None)
    return [prev_page, next_page]


class VnsGameParser(GameParser):
    def __init__(self, days):
        super().__init__(days)

    def get_relatable_resources(self):
        PAG = SELECTORS['pagination']

        def tryload(link):
            html = NoProxyHTTPFetcher.fetch(link, headers=HEADERS)
            soup = bs4.BeautifulSoup(html, 'html.parser')
            return [html, soup]

        def safeload(link, validator, *args):
            html, soup = tryload(link)
            while not (retval := validator(soup, *args)):
                logger.error(f"Validating {link} failed, retrying...")
                html, soup = tryload(link)
            return (retval, html, soup)

        for no, link in self.days.items():
            pagedict = dict()
            retval, html, soup = safeload(link, ftext, PAG['current'])
            pagedict[int(retval)] = (link, html, soup)

            prev_page, next_page = get_prev_and_next(soup, retval)
            while next_page:
                logger.info(f"Fetching {next_page}")
                retval, html, soup = safeload(
                    next_page[1], get_prev_and_next, next_page[0])
                pagedict[next_page[0]] = (next_page[1], html, soup)
                next_page = retval[1]

            while prev_page:
                logger.info(f"Fetching {prev_page}")
                retval, html, soup = safeload(
                    prev_page[1], get_prev_and_next, prev_page[0])
                pagedict[prev_page[0]] = (prev_page[1], html, soup)
                prev_page = retval[0]

            self.pagedicts.append(pagedict)

    def parse_gameinfo(self):
        pass

    def parse_all_days(self):
        comments = list()
        for dayno, pagedict in enumerate(self.pagedicts):
            logger.info(f"Parsing day {dayno + 1}")
            pagesoups = [pagedict[key][2] for key in sorted(pagedict.keys())]
            numdict = {
                'day': dayno + 1,
                'post': 1
            }
            for pageno, pagesoup in enumerate(pagesoups):
                numdict['page'] = pageno + 1
                comments += self.parse_page(pagesoup, numdict)
        self.game.comments = comments

    def export_all_htmls(self, dirname='temp'):
        path = njoin(os.path.dirname(__file__), f'../{dirname}')
        if not os.path.exists(path):
            os.makedirs(path)
        for dayno, pagedict in enumerate(self.pagedicts):
            for pageno, content in pagedict.items():
                name = f"day-{dayno + 1}-page-{pageno}.html"
                logger.info(f"Writing content to {name}")
                with open(njoin(path, name), 'w+', encoding='utf8') as f:
                    f.write(content[1])

    def load_from_res(self, path):
        def singleload(path):
            with open(path, 'r', encoding='utf8') as f:
                html = f.read()
                soup = bs4.BeautifulSoup(html)
        if not (htmlfiles := [
                file_ for file_ in os.listdir(path)
                if file_.endswith('.html')]):
            raise ValueError(f"Can not file a single html file from {path}")
