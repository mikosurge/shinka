from shinka.emarchiver.unit.game import Game


class GameParser():
    def __init__(self, days):
        self.days = days
        self.pagedicts = list()
        self.game = Game()

    def get_relatable_resources(self):
        """Returns all tuples mapping days and
        its hyperlink, html content and parsed soup.
        """
        pass

    def parse_gameinfo(self):
        """Updates the game metadata to the parser's game dict.
        """
        pass

    def parse_page(self):
        """Parses of a soup page into comment objects.
        """
        pass

    def parse_day(self):
        """Parses a day of many pages into comment objects.
        """
        pass

    def parse_all_days(self):
        """Parses all days stored in the parser's information.
        """
        pass

    def parse(self):
        self.get_relatable_resources()
        self.parse_gameinfo()
        self.parse_all_days()
        return self.game
