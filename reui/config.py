import json
from pathlib import Path
from typing import Any, Dict

from pydantic import (
    BaseSettings,
    DirectoryPath,
    FilePath,
    validator
)


class Settings(BaseSettings):
    ROOT_DIR: DirectoryPath = Path(__file__).absolute().parent
    RES_DIR: DirectoryPath = "res"
    SRC_DIR: DirectoryPath = "src"
    TMP_DIR: DirectoryPath = "tmp"

    @validator("RES_DIR", "SRC_DIR", "TMP_DIR", pre=True)
    def apply_root(cls, v, values):
        if root_dir := values.get("ROOT_DIR"):
            res_path = root_dir / v
            res_path.mkdir(parents=True, exist_ok=True)
            return res_path
        return v

    SCALE_RATIO: 1

    REUI_RESOURCE: FilePath = "config.json"

    @validator("REUI_RESOURCE", pre=True)
    def apply_res(cls, v, values):
        if res_dir := values.get("RES_DIR"):
            return res_dir / v
        return v

    REUI_CONFIG: Dict[str, Any] = {}

    @validator("REUI_CONFIG")
    def load_config(cls, v, values):
        reui_res = values.get("REUI_RESOURCE")
        with open(reui_res, "r") as f:
            v = json.load(f)
        assert v
        return v


settings = Settings()
