import itertools
import tkinter as tk
import tkinter.font as tkfont
from shinka.utils.common import flatten
from shinka.utils.widget import insert_vertically, insert_horizontally, \
    LineSeparator, Frame, Label


def extract_attrs(attrs: dict = dict()):
    ret = list()
    for key in attrs.keys():
        if isinstance(attrs[key], list):
            ret += attrs[key]
        elif isinstance(attrs[key], str):
            ret.append(attrs[key])
    return ret


class WordFrame(tk.Frame):
    def __init__(self, master, name, data):
        super().__init__(master)
        self.master = master
        self._name = name
        self._data = data

        self.rowconfigure([0, 2, 4], weight=1, minsize=32)
        self.columnconfigure(0, weight=1, minsize=512)

        basefont = tkfont.nametofont('TkDefaultFont')
        basefamily = basefont.cget('family')

        self.font_h1 = tkfont.Font(family=basefamily, size=24)
        self.font_h5i = tkfont.Font(family=basefamily, size=12, slant='italic')
        self.font_h6 = tkfont.Font(family=basefamily, size=11, weight='bold')
        self.font_p = tkfont.Font(family=basefamily, size=10)
        self.font_pb = tkfont.Font(family=basefamily, size=9, weight='bold')
        self.font_pi = tkfont.Font(family=basefamily, size=10, slant='italic')
        self.font_si = tkfont.Font(family=basefamily, size=8, slant='italic')

        self._header = Frame(self, 'header', padx=10)
        self._header.columnconfigure(1, weight=1)
        self._header.grid(row=0, column=0, sticky='nsew')
        self._ls1 = LineSeparator(self, width=512 - 64)
        self._ls1.grid(row=1, column=0)
        self._body = Frame(self, 'body', padx=10)
        self._body.columnconfigure(0, weight=1)
        self._body.grid(row=2, column=0, sticky='nsew')
        self._ls2 = LineSeparator(self, width=512 - 64)
        self._ls2.grid(row=3, column=0)
        self._footer = Frame(self, 'footer', padx=10, pady=5)
        self._footer.columnconfigure(0, weight=1)
        self._footer.grid(row=4, column=0, sticky='nsew')

    def config_cb(self, event):
        self._ls1.config(width=self.winfo_width() - 128)
        self._ls2.config(width=self.winfo_width() - 128)

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    def set_header(self):
        worddict = self.data
        word = worddict.get('word')
        type_ = worddict.get('type')
        pron = worddict.get('pronunciations', dict())

        lb_word = Label(
            self._header, 'header_word', font=self.font_h1, text=word)
        lb_word.grid(row=0, column=0, sticky='w')
        lb_type = Label(
            self._header, 'header_type', font=self.font_h5i, text=f'({type_})')
        lb_type.grid(row=1, column=0, sticky='w', padx=(10, 0))
        fr_pron = Frame(self._header, 'frame_pron')
        fr_pron.grid(row=1, column=1, sticky='e')
        fr_attr = Frame(self._header, 'frame_attr')
        fr_attr.grid(row=0, column=1, sticky='se')

        pronwdgs = flatten([
            [
                tk.Label(fr_pron, text=k.upper(), borderwidth=1,
                         relief='solid', anchor='e',
                         padx=1, font=self.font_pb),
                tk.Label(fr_pron, text=v, anchor='w', padx=1)
            ]
            for k, v in pron.items() if v
        ])
        insert_horizontally(pronwdgs, padx=2)

        attrwdgs = [
            tk.Label(fr_attr, font=self.font_pb, bg='black',
                     fg='white', text=attr, padx=4, bd=0)
            for attr in extract_attrs(worddict.get('attrs', dict()))
        ]
        insert_horizontally(attrwdgs, padx=2)

    def set_body(self):
        worddict = self.data
        definition = worddict.get('definition')
        examples = worddict.get('examples')

        lb_defn = Label(self._body, 'body_definition', wraplength=512,
                        justify='left', font=self.font_h6, text=definition)
        lb_defn.grid(row=0, column=0, padx=(10, 0), pady=5, sticky='w')
        fr_exam = Frame(self._body, 'body_exam')
        fr_exam.grid(row=1, column=0, padx=(20, 0), sticky='nsew')

        if examples:
            examwdgs = [
                Label(fr_exam, f'body_exam_{i}', wraplength=512,
                      justify='left', font=self.font_pi,
                      text=f'\N{BULLET} {e}')
                for i, e in enumerate(examples)
            ]
            insert_vertically(examwdgs, pady=2, sticky='w')
        return self

    def set_footer(self):
        worddict = self.data
        topics = worddict.get('related_topics', list())
        rels = worddict.get('relations', dict())
        source = worddict.get('source')

        if rels:
            fr_rels = Frame(self._footer, 'footer_relations')
            fr_rels.grid(row=0, column=0, sticky='nsew')
            fr_rels.columnconfigure(0, minsize=128)
            fr_rels.columnconfigure(1, weight=1)
            rel_row = 0
            for k, v in rels.items():
                lbl = Label(fr_rels, f'left_rel_{rel_row}', justify='left',
                            padx=8, font=self.font_pb, text=k)
                lbl.grid(row=rel_row, column=0, sticky='ew')
                lbr = Label(fr_rels, f'right_rel_{rel_row}', justify='left',
                            anchor='w', font=self.font_p,
                            wraplength=256, text=', '.join(v))
                lbr.grid(row=rel_row, column=1, padx=8, sticky='ew')
                rel_row += 1

        if topics:
            fr_tpcs = Frame(self._footer, 'footer_topics')
            fr_tpcs.grid(row=0, column=0, sticky='nsew')
            fr_tpcs.columnconfigure(0, minsize=128)
            fr_tpcs.columnconfigure(1, weight=1)
            lbl = Label(fr_tpcs, 'left_topics', justify='left',
                        padx=8, font=self.font_pb, text="Related topics")
            lbl.grid(row=0, column=0, sticky='ew')
            frr = Frame(fr_tpcs, 'right_topics')
            frr.grid(row=0, column=1, padx=8, sticky='ew')
            lbrs = [
                Label(frr, f'topic_{i}', justify='left',
                      font=self.font_p, text=t.get('name'))
                for i, t in enumerate(topics)]
            insert_vertically(lbrs, pady=1, sticky='ew')

        fr_org = Frame(self._footer, 'footer_origin')
        fr_org.grid(row=2, column=0, sticky='nsew')
        fr_org.columnconfigure([0, 1], weight=1)
        lb_src = Label(fr_org, 'source', justify='left',
                       font=self.font_si, text=source)
        lb_src.grid(row=0, column=0, sticky='w')
        lb_org = Label(fr_org, 'origin', justify='right',
                       font=self.font_si, text='Syndicate')
        lb_org.grid(row=0, column=1, sticky='e')
        return self

    def construct(self):
        self.set_header()
        self.set_body()
        self.set_footer()
        return self
