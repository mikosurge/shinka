# import json
import tkinter as tk
from datetime import timedelta
from random import randint
from timeloop import Timeloop
from frame.word import WordFrame
from shinka.wordshard.accumulator import all_shards


__data = None


def get_random():
    global __data
    if not __data:
        # with open('words.json', 'r', encoding='utf8') as f:
        #     __data = json.load(f)
        __data = all_shards
    return __data[randint(0, len(__data) - 1)]


class SampleApp(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.timeloop = Timeloop()
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.grid(row=0, column=0, sticky='nsew')
        self.create_widgets()

    def create_widgets(self):
        self.frame = WordFrame(self, 'word', get_random()).construct()
        self.frame.grid(row=0, column=0, sticky='nsew')


tl = Timeloop()


@tl.job(interval=timedelta(seconds=10))
def change_word():
    global app
    old_app = app
    app = SampleApp(master=root)
    old_app.destroy()


root = tk.Tk()
root.rowconfigure(0, weight=1)
root.columnconfigure(0, weight=1)
app = SampleApp(master=root)
tl.start(block=False)
app.mainloop()
