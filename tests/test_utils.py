import pytest
from typing import Any

from shinka.utils.shared import trim_falsy, merge


@pytest.mark.parametrize(
    "given_value, result",
    [
        (
            {
                "a": "a", "b": 1, "c": True,
                "x": 0, "y": None, "z": [], "t": {}
            },
            {"a": "a", "b": 1, "c": True},
        ),
        (
            ["a", 1, True,  0, None, [], {}],
            ["a", 1, True],
        ),
        ("foobarbazqux", "foobarbazqux"),
    ]
)
def test_trim_falsy(
        given_value: Any,
        result: Any,
        ):
    assert trim_falsy(given_value) == result


class Dest:
    a = {"b": "foo", "c": "bar"}
    b = ["foo", "bar"]
    c = "foobar"
    d = "barqux"
    e = "quz"


def test_merge():
    dst = {
        "a": {"b": "foo", "c": "bar"},
        "b": ["foo", "bar"],
        "c": "foobar",
        "d": "barqux",
        "e": "quz",
    }
    src = {
        "a": {"d": "qux", "c": "bar"},
        "b": ["bar", "qux"],
        "c": "barfoo",
        "d": None,
    }
    dst = Dest()
    dst.b = ["foo", "bar", "qux"]
    MergedDest = merge(Dest(), src)
    assert MergedDest.a == {"b": "foo", "c": "bar", "d": "qux"}
    assert MergedDest.b == ["foo", "bar", "qux"]
    assert MergedDest.c == "barfoo"
    assert MergedDest.d == "barqux"
    assert MergedDest.e == "quz"
