import hashlib
import random
import uuid


def md5(filename):
    hash_md5 = hashlib.md5()
    with open(filename, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def unique(length=None):
    if length:
        return uuid.uuid4().hex[:length]
    else:
        return uuid.uuid4().hex[:]


def random_hexcolor():
    def r():
        return lambda: random.randint(0, 255)
    return '#%02X%02X%02X' % (r(), r(), r())
