import re
import json


formatdict = {
    "bold": "**{content}**",
    "italics": "*{content}*",
    "underline": "__{content}__",
    "strikethrough": "~~{content}~~",
    "blockquote": "> {content}\n",
    "ul": "{content}\n",
    "ol": "{content}\n",
    "li": "{content}",
    "li-unordered": "- {content}",
    "li-ordered": "{sub}. {content}",
    "link": "[{content}]({sub})",
    "h1": "# {content}",
    "h2": "## {content}",
    "h3": "### {content}",
    "codeblock-inline": "`{content}`",
    "codeblock": "```\n{content}\n```",
    "line-break": "",
    "horizontal-rule": "___",
    "hidden": "",
    "color": "<span style=\"{sub}\">{content}</span>",
    "image": "![{content}]({sub})",
    "center": "<center>{content}</center>",
    "left": "{content}"
}


def format_md(content, type_, sub):
    if formatdict.get(type_):
        raw = content.strip(' ')
        if raw or type_ == 'horizontal-rule':
            leftspaces = len(content) - len(content.lstrip(' '))
            rightspaces = len(content) - len(content.rstrip(' '))
            return ' ' * leftspaces + \
                formatdict[type_].format(content=raw, sub=sub) + \
                ' ' * rightspaces
    return content


def fix_escape_chars(text):
    return re.sub(r'([\*\>\-\+])', r'\\\1', text)


def extract_text(md):
    # TODO Implement it!
    pass
