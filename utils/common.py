import colorlog
import itertools
import json
import logging
import os

from shinka.config import settings

ROOTDIR = settings.ROOT_DIR
LOGCONF = settings.LOGGING_CONFIG


def init_logger(dunder_name) -> logging.Logger:
    log_format = (
        '%(asctime)s - '
        '%(name)s - '
        '%(funcName)s - '
        '%(levelname)s - '
        '%(message)s'
    )
    bold_seq = '\033[1m'
    colorlog_format = (
        f'{bold_seq} '
        '%(log_color)s '
        f'{log_format}'
    )
    colorlog.basicConfig(format=colorlog_format)
    logger = logging.getLogger(dunder_name)

    if LOGCONF.get(dunder_name, False):
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    default_name = LOGCONF.get('default')
    debug_path = ROOTDIR / f"{default_name}.log"
    warning_path = ROOTDIR / f"{default_name}.warn.log"
    error_path = ROOTDIR / f"{default_name}.error.log"

    # Output
    if LOGCONF.get('__full_log', False):
        # Overwrite old log file for each session
        fh = logging.FileHandler(debug_path, 'w+', 'utf8')
        fh.setLevel(logging.DEBUG)
        formatter = logging.Formatter(log_format)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    if LOGCONF.get('__warn_log', False):
        # Overwrite old log file for each session
        fh = logging.FileHandler(warning_path, 'w+', 'utf8')
        fh.setLevel(logging.WARNING)
        formatter = logging.Formatter(log_format)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    if LOGCONF.get('__error_log', False):
        # Overwrite old log file for each session
        fh = logging.FileHandler(error_path, 'w+', 'utf8')
        fh.setLevel(logging.ERROR)
        formatter = logging.Formatter(log_format)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    return logger


def njoin(left, right):
    return os.path.normpath(os.path.join(left, right))


def compare_obj(a, b):
    if type(a) != type(b):
        return False
    elif isinstance(a, dict):
        return compare_dict(a, b)
    elif isinstance(a, list):
        return compare_list(a, b)
    else:
        return a == b


def compare_dict(a, b):
    if len(a) != len(b):
        return False
    else:
        for k, v in a.items():
            if k not in b:
                return False
            else:
                if not compare_obj(v, b[k]):
                    return False
    return True


def compare_list(a, b):
    if len(a) != len(b):
        return False
    else:
        for i in range(len(a)):
            if not compare_obj(a[i], b[i]):
                return False
    return True


def truthy_dict(dict_):
    if not dict_:
        return None
    return {
        k: v
        for k, v in dict_.items()
        if v
    }


def truthy_list(list_):
    if not list_:
        return None
    return list(filter(lambda i: i, list_))


def stringify(dict_, filename=None, indent=2):
    if filename:
        with open(filename, 'w+', encoding='utf8') as f:
            json.dump(dict_, f, indent=indent, ensure_ascii=False)
    else:
        return json.dumps(dict_, indent=indent, ensure_ascii=False)


def flatten(iterable):
    return list(itertools.chain.from_iterable(iterable))


def safeint(value, default=None):
    try:
        return int(value)
    except ValueError:
        return default
