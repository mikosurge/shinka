def first(node, selector):
    """Returns the first occurrence of a CSS Selector search.

        This attempt is based on ``select`` method of ``bs4.BeautifulSoup``
    """
    elements = node.select(selector)
    return elements[0] if elements else None


def ftext(node, selector, fmt=None):
    """Returns the stripped text of the first occurrence.
    """
    element = first(node, selector)
    text = element.get_text().strip() if element else None
    return fmt(text) if fmt else text


def textlist(node, selector):
    """Returns raw text array retrieved from multiple nodes of the same kind.
    """
    return [
        child.get_text().strip()
        for child in node.select(selector)
    ]
