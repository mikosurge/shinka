import os
import pickle


def load_from_pickle(path):
    data = None
    if os.path.exists(path):
        with open(path, 'rb') as f:
            data = pickle.load(f)
    return data


def save_to_pickle(path, data):
    with open(path, 'wb') as f:
        pickle.dump(data, f)
