import asyncio
from concurrent.futures import ThreadPoolExecutor
import json
import os
from random import randint

from shinka.shared.exporter import PlainExporter
from shinka.utils.common import init_logger, njoin
from shinka.config import settings


logger = init_logger(__name__)
MAX_ITEMS = settings.PULSE_MAX_ITEMS
MAX_DELAY = settings.PULSE_MAX_DELAY
MAX_GAP_TIME = settings.PULSE_MAX_GAP_TIME
TMPDIR = njoin(os.path.dirname(__file__), 'temp')


def tolerate_exc(item):
    if is_exc := isinstance(item, Exception):
        logger.warning(item)
    return not is_exc


async def random_gap(max_pulse_gap: int = MAX_GAP_TIME):
    gap = randint(1, max_pulse_gap)
    logger.debug(f"Entering {gap} seconds gap")
    await asyncio.sleep(gap)


async def mock(item):
    delay = randint(1, MAX_DELAY)
    logger.debug(f"[@MOCK] Will sleep for {delay} second(s)")
    await asyncio.sleep(delay)
    logger.debug(f"[@MOCK] Done processing {item}")


async def wrap_in_threadpool(fn, *args):
    loop = asyncio.get_event_loop()
    with ThreadPoolExecutor() as pool:
        await loop.run_in_executor(pool, fn, *args)


def generate_tasks(fn, items):
    return [
        asyncio.create_task(wrap_in_threadpool(fn, item))
        for item in items
    ]


# https://www.roguelynn.com/words/asyncio-exception-handling/
# TODO Seperate mapping results with exceptions to a function
async def gather_with_exc(tasks: list, incompletes: list):
    return(list(map(
        tolerate_exc, await asyncio.gather(*tasks, return_exceptions=True))))


async def pulse(fn, items, incompletes, exportdir):
    tasks = generate_tasks(fn, items)
    results = await gather_with_exc(tasks, incompletes)
    if _incompletes := [
            items[i] for i, res in enumerate(results) if not res
            ]:
        incompletes += _incompletes
        logger.warning(
            f"[@PULSE] New incomplete cases: {_incompletes}, exporting...")
        PlainExporter.export(
            njoin(exportdir, '__incompletes.json'),
            json.dumps(incompletes, indent=2, ensure_ascii=False)
        )
    return results


async def random_pulse(fn,
                       items: list,
                       exportdir: str,
                       max_items: int = MAX_ITEMS,
                       max_pulse_gap: int = MAX_GAP_TIME):
    total = len(items)
    current = 0
    incompletes = list()

    while True:
        n = randint(1, max_items)
        if current + n >= total - 1:
            logger.debug("[@PULSE] Processing remaining items")
            await pulse(fn, items[current:], incompletes, exportdir)
            break
        else:
            logger.debug(f"[@PULSE] Processing {n} items")
            await pulse(fn, items[current:current + n], incompletes, exportdir)
            await random_gap(max_pulse_gap)
            current += n
