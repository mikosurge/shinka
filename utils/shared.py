from collections.abc import Mapping, Sequence
from typing import Any


def trim_falsy(arg: Any):
    type_ = type(arg)
    if isinstance(arg, Mapping):
        res = type_({
            k: v
            for k, v in arg.items()
            if v
        })
    elif isinstance(arg, Sequence) and not isinstance(arg, str):
        res = type_([i for i in arg if i])
    else:
        res = arg
    return res


def merge_list(dst, src):
    """
    Merges two list, output only distinct members.
    """
    [
        dst.append(x)
        for x in src
        if x not in dst
    ]
    return dst


def merge_dict(dst, src):
    """
    Merges two dict, the src dict takes precedence.
    This method ran in x2 speed as using spread operator.
    """
    dst.update(src)
    return dst


def merge(dst, src):
    """
    Merges a dict containing attributes to a class.

    Args:
        dst: An arbitrary class
        src (dict): A dict contains keys for updating
    """
    if not dst:
        return src
    for k, v in src.__dict__.items():
        if not v:
            continue
        if (a := getattr(dst, k, None)):
            if isinstance(a, list):
                _a = merge_list(a, v)
            elif isinstance(a, dict):
                _a = merge_dict(a, v)
            else:
                _a = v
            setattr(dst, k, _a)
    return dst
