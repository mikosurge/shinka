import tkinter as tk

from shinka.config import settings


DEBUG = settings.DEBUG


def insert_vertically(widgets: list = list(),
                      col: int = 0,
                      start_row: int = 0,
                      gap: int = 1,
                      **kwargs):
    for widget in widgets:
        widget.grid(row=start_row, column=col, **kwargs)
        start_row += gap


def insert_horizontally(widgets: list = list(),
                        row: int = 0,
                        start_col: int = 0,
                        gap: int = 1,
                        **kwargs):
    for widget in widgets:
        widget.grid(row=row, column=start_col, **kwargs)
        start_col += gap


class LineSeparator(tk.Frame):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.config(height=1, bg='black')


class Frame(tk.Frame):
    def __init__(self, master, name, debug=DEBUG, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self._name = name
        if debug:
            self.config(highlightbackground='red', highlightthickness=1)


class Label(tk.Label):
    def __init__(self, master, name, debug=DEBUG, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self._name = name
        if debug:
            self.config(borderwidth=1, relief='solid')
        self.bind('<Configure>', self.config_cb)

    def config_cb(self, event):
        self.config(wraplength=self.winfo_width())
